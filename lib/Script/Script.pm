#/usr/bin/perl

{
    package Script::Script;
	use Moose;

	use Getopt::Long;
	use List::MoreUtils qw(first_index indexes);

	has 'arguments' => (
	    isa => 'ArrayRef',
	    is => 'rw',
	    default => sub { [] }
	);

	has 'options' => (
		isa => 'HashRef',
		is => 'rw',
		traits => ['Hash'],
		handles => {
		    'set_opt' => 'set',
		    'get_opt' => 'get',
		    'has_opt' => 'is_empty',
		    'get_options' => 'keys',
		    'delete_opt' => 'delete',
		    'pairs' => 'kv',
		},
		default => sub { {} }
	);

	sub BUILDARGS {
	    my ($self, $options) = @_;

	    # Prepare options hash
	    my $opt = {};
	    my $args;
	    {
            my %opt;
            map {
                my $val = '';
                $opt{$_} = \$val;
            } @$options;

            # Get options
            GetOptions(%opt);

            # Get arguments
            $args = \@ARGV;

            # Dereference values and remove control characters from keys.
            while (my ($o, $value) = each(%opt)) {
                $o =~/^([\w -]{1,})([+:=]*)(.*)$/;
                $opt->{$1} = $$value;
            }
        }

	    return {options => $opt, arguments => $args};
	}

	sub BUILD {
	    my ($self, $args) = @_;
	}

	sub main {
	    my ($self) = @_;
	    die "Implement your own main method!";
	}

	sub run {
	    my ($class, $options) = @_;
	    my $instance = $class->new($options);
	    return $instance->main();
	}

	no Moose;
	__PACKAGE__->meta->make_immutable;
}

1;

