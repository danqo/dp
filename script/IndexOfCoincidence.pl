#/usr/bin/perl

{
    package IndexOfCoincidence;
    use Moose;
    extends 'Script::Script';

    sub main {
        my ($self) = @_;

        my @alphabet = (
            'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r',
            's', 't', 'u', 'v', 'w', 'x',
            'y', 'z',
        );

        my $data = {};
        map {$data->{$_} = 0} @alphabet;
        my $total_len;

        open (my $f, '<', $self->get_opt('input')) or die "Unable to open file: " . $self->get_opt('input');
        my $char;
        while ((read $f, $char, 1, 0) != 0) {
            if (!exists $data->{$char}) {
                print "Ignore character: $char\n";
                next;
            }
            $data->{lc $char} += 1;
            $total_len += 1;
        }

        my $sum;
        while(my($char, $value) = each %$data) {
            $sum += $value * ($value - 1);
        }
        no integer;
        my $result = $sum / ($total_len * ($total_len -1));
        print "Index of coincidence: $result\n";
        use integer;
    }

    no Moose;
    __PACKAGE__->meta->make_immutable;
}

# Options documentation:
# http://perldoc.perl.org/Getopt/Long.html

my $options = [
    'input=s',
];
IndexOfCoincidence->run($options);

1;
