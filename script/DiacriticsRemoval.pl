#/usr/bin/perl

{
    package DiacriticsRemoval;
    use Moose;
    extends 'Script::Script';
    use POSIX qw(locale_h);

    has 'map' => (
        isa => 'HashRef',
        is => 'rw',
        traits => ['Hash'],
        handles => {
            'set_key' => 'set',
            'convert' => 'get',
            'exist_key' => 'exists',
            'remove_key' => 'delete',
            'get_keys' => 'keys',
            'map_pairs' => 'kv'
        },
        lazy => 1,
        builder => '_build_map'
    );

    sub main {
        my ($self) = @_;
        use utf8;
        binmode STDOUT, "utf8";
        binmode STDIN, "utf8";

        $self->convert_file();
    }

    sub _build_map {
        my ($self) = @_;
        open FILE, "<:utf8", $self->get_opt('map') or 
            die "Unable to open map file: " . $self->get_opt('map');

        # Read and set locale
        my $line = <FILE>;
        die "Bad file format in line \"$line\"" if ($line !~ /^locale,.{1,}$/);
        my $locale = (split ",", $line)[1];
        print "Setting locale: $locale\n";
        setlocale(LC_CTYPE, $locale);

        my $result = {};
        foreach $line (<FILE>) {
            #TODO: check line format
            #die "Bad line format: \"$line\"" if ($line !~ /^\w(:\w){1,},\w$/);
            # Remove \n\r.
            $line =~ s/\r+//;
            $line =~ s/\n+//;
            my @line = split ":", $line;
            my @keys = split ",", $line[1];

            for my $key (@keys) {
                die "Duplicated key: $key" if(exists $result->{$key});
                #printf $key . " => " . $line[0] . "\n";
                $result->{$key} = $line[0];
            }
        }
        close (FILE);
        return $result;
    }

    sub help {
        print "Tool to remove diacritics from text.\n";
        # TODO: write help
    }

    sub convert_file() {
        my ($self) = @_;

        if (!$self->get_opt('input')) {
            die "--input is not specified";
        }

        if (!$self->get_opt('output')) {
            die "--output is not specified";
        }

        open INPUT, "<:utf8", $self->get_opt('input') or
            die "Unable to open/create file: " .
                $self->get_opt('input') . "\n";

        open OUTPUT, ">:utf8", $self->get_opt('output') or 
            die "Unable to open/create file: " .
                $self->get_opt('output') . "\n";

        my $char;
        while ((read INPUT, $char, 1, 0) != 0) {
            next if ($char =~ /\s/ && $self->get_opt('remove-whitespace'));
            $char = lc $char;
            if ($self->exist_key($char)) {
                $char = $self->convert($char);
            } else {
                next if ($self->get_opt('ignore-unmaped'));
            }
            printf OUTPUT uc $char;
        }
        close(INPUT);
        close(OUTPUT);
    }
    no Moose;
    __PACKAGE__->meta->make_immutable;
}

# Options documentation:
# http://perldoc.perl.org/Getopt/Long.html

my $options = [
    'remove-whitespace',
    'ignore-unmaped',
    'map=s',
    'input=s',
    'output=s'];
DiacriticsRemoval->run($options);

1;
