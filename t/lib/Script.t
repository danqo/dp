#/usr/bin/perl

{
    package Test;
    use Moose;
    extends 'Script::Script';
    
    sub main {
        my ($self) = @_;
    }
    
    no Moose;
    __PACKAGE__->meta->make_immutable;
}

my $options = ['option', 'param1=s'];
Test->run($options);

1;

