using System;
using swarmLib;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace swarm
{
	class MainClass
	{
		public static void Main (string[] args) {
			if (args.Length == 0) {
				help();
				return;
			} else if (args[0].Equals("--help") || args[0].Equals("-h")) {
				help();
				return;
			}
			Console.WriteLine("Running: {0}", args[0]);
			HeuristicFactory factory = new HeuristicFactory(Utils.STA);
			string path = args[0];
			List<TestCase> cases = factory.ReadTestCases(path);
			List<Hashtable> data = new List<Hashtable>();
			List<Thread> threads = new List<Thread>();

			foreach(TestCase t in cases) {
				Thread thread = new Thread(new ParameterizedThreadStart(RunCase));
				thread.Start(new object[] {t, data});
				threads.Add(thread);
			}

			foreach(Thread thread in threads) {
				thread.Join();
			}
		}

		public static  void RunCase(object o) {
			object[] param = (object[]) o;
			TestCase c = (TestCase) param[0];
			List<Hashtable> data = (List<Hashtable>) param[1];
			Hashtable d = c.Run();
			c.Report(d);
			c.SaveData(d, null);
			data.Add(d);
		}

		public static void help() {
			Console.WriteLine("Usage: mono swarm.exe testcase.xml");
		}
	}
}