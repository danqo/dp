using System;
using NUnit.Framework;
using swarmLib;
using System.Collections.Generic;
using System.IO;


namespace t
{
	[TestFixture()]
	public class Test {
		[Test()]
		public void PermutationTest () {
			Permutation a = new Permutation(new int[] {1, 2, 3, 0});
			Permutation b = new Permutation(new int[] {2, 0, 3, 1});
			
			// ApplyTo
			object[] data = new object [4] {1, 2, 3, 4};
			object[] expect = new object[4] {3, 1, 4, 2};
			data = b.ApplyTo(data);
			Assert.AreEqual(expect, data);
			
			// IsValid
			Assert.IsTrue(a.IsValid());
			
			// Inverse
			object[] toInverse = new object[] {0, 1, 2, 3, 4};
			Permutation inv = new Permutation(new int[] {1, 0, 2, 4, 3});
			object[] doubleInverted = inv.Inverse().ApplyTo(inv.ApplyTo(toInverse));
			for (int i = 0; i < toInverse.Length; i++) 
			{
				Assert.AreEqual(toInverse[i], doubleInverted[i]);
			}

			// Composition
			Permutation c = a.ComposeWith(b);
			Permutation d = new Permutation(new int[] {3,1,0,2});
			Assert.AreEqual(d.ToString(), c.ToString());
			
			// Cycles
			List<Permutation> permutations = new List<Permutation>();
			List<string> answers = new List<string>();
			
			permutations.Add(new Permutation(new int[] {0, 2, 3, 1, 4}));
			answers.Add("(0)(1 2 3)(4)");
			permutations.Add(new Permutation(new int[] {1, 2, 0, 4, 3}));
			answers.Add("(0 1 2)(3 4)");
			permutations.Add(new Permutation(new int[] {0, 1, 2, 3, 4}));
			answers.Add("(0)(1)(2)(3)(4)");
			permutations.Add(new Permutation(new int[] {4, 3, 2, 1, 0}));
			answers.Add("(0 4)(1 3)(2)");
			permutations.Add(new Permutation(new int[] {1, 2, 3, 4, 0}));
			answers.Add("(0 1 2 3 4)");
			
			for(int i = 0; i < permutations.Count; i++) {
				Assert.AreEqual(answers[i], permutations[i].CycleNotation());
			}
			
			// Permutation order
			Assert.AreEqual(9, a.GetOrder());
			Assert.AreEqual(13, b.GetOrder());
			Assert.AreEqual(0, new Permutation(new int[] {0,0,0,0}).GetOrder());
			Assert.AreEqual(23, new Permutation(new int[] {3,2,1,0}).GetOrder());

			// Clone
			Permutation toClone = new Permutation(4,10);
			Permutation cloned = (Permutation) toClone.Clone();
			Assert.AreEqual(toClone.ToString(), cloned.ToString());
		}
		
		[Test()]
		public void TranspositionTest() {
			Permutation p = new Permutation(new int[] { 3, 0, 1, 7, 8, 4, 2, 6, 9, 5 });
			PermutationKey encryptKey = new PermutationKey(p);
			PermutationKey decryptKey = new PermutationKey(p.Inverse());
			
			object[] plain = new object[10] {'H','e','l','l','o','W','o','r','l','d'};
			
			Transposition transposition = new Transposition();
			object[] close = transposition.Encrypt(plain, encryptKey);
			
			int i = 0;
			foreach(object o in transposition.Decrypt(close, decryptKey))
			{
				Assert.AreEqual(plain[i++], o);
			}
		}
		
		[Test()]
		public void BlockTranspositionTest() {
			Permutation p = new Permutation(new int[] { 1, 3 ,2, 0});
			PermutationKey encryptKey = new PermutationKey(p);
			PermutationKey decryptKey = new PermutationKey(p.Inverse());
			
			object[] plain = new object[10] {'H','e','l','l','o','W','o','r','l','d'};
			object[] expected = new object[12] {'H','e','l','l','o','W','o','r','l','d','0','0'};
			
			BlockTransposition transposition = new BlockTransposition(4);
			transposition.PaddingValue = '0';
			object[] close = transposition.Encrypt(plain, encryptKey);
			object[] decrypted = transposition.Decrypt(close, decryptKey);
			
			int i = 0;
			foreach(object o in expected)
			{
				Assert.AreEqual(o, decrypted[i++]);
			}
		}

		[Test()]
		public void VigenereTest() {
			VigenereKey k = new VigenereKey(new object[] {'H','E','S','L','O'});
			object[] plain = new object[7] {'B','O','S','O','R','K','A'};
			object[] expectedClose = new object[7] {'I','S','K','Z','F','R','E'};

			Vigenere v = new Vigenere();
			object[] close = v.Encrypt(plain, k);
			int i = 0;
			foreach(object o in expectedClose) {
				Assert.AreEqual(o, close[i++]);
			}

			i = 0;
			object[] decrypt = v.Decrypt(close, k);
			foreach(object o in decrypt) {
				Assert.AreEqual(o, plain[i++]);
			}
		}

		[Test()]
		public void HomophonicSubstitutionTest() {
			HomophonicSubstitutionKey key = new HomophonicSubstitutionKey();
			HomophonicSubstitution hs = new HomophonicSubstitution();

			key.AddKey('A', new object[] {'X', 'Y', 'Z'});
			key.AddKey('B', new object[] {'F', 'G', 'H', 'I', 'J'});
			key.AddKey('C', new object[] {'K', 'L', 'M', 'N', 'O', 'P'});
			key.AddKey('D', new object[] {'Q', 'R', 'S'});

			object[] plain = new object[7] {'A','B','C','D','D','B','C'};
			object[] closed = hs.Encrypt(plain, key);
			object[] decrypt = hs.Decrypt(closed, key);

			int i = 0;
			foreach(object o in decrypt) {
				Assert.AreEqual(o, plain[i++]);
			}
		}

		[Test()]
		public void MonoalphabeticSubstitutionTest() {
			MonoalphabeticSubstitution ms = new MonoalphabeticSubstitution();

			Permutation p = new Permutation(26, 587545568);
			Permutation pi = p.Inverse();

			MonoalphabeticSubstitutionKey ek = new MonoalphabeticSubstitutionKey(p);
			MonoalphabeticSubstitutionKey dk = new MonoalphabeticSubstitutionKey(pi);

			object[] plain = new object[7] {'A','B','S','T','X','Y','Z'};
			object[] closed = ms.Encrypt(plain, ek);
			object[] decrypt = ms.Decrypt(closed, dk);

			int i = 0;
			foreach(object o in decrypt) {
				Assert.AreEqual(o, plain[i++]);
			}

		}

		[Test()]
		public void DictionaryTest() {
			ICipher c = new BlockTransposition(5);
			((BlockTransposition) c).PaddingValue = 'A';
			Evaluation eval = new DictionaryEvaluation(c, "data/slovak_dict.txt", 4);
			object[] text = {
				'S', 'T', 'A', 'T', 'I', 'S', 'T', 'I', 'K', 'A',
				'S', 'L', 'O', 'V', 'O', 'X', 'X', 'S', 'I', 'F',
				'R', 'A'
			};
			Permutation p = new Permutation(5, 40);
			IKey k = new PermutationKey(p);
			object[] closedText = c.Encrypt(text, k);
			double d = eval.Evaluate(closedText, (Solution) p.Inverse());
			Assert.AreEqual(170, d);
		}

		//[Test()]
		public void BigramStatisticTest() {
			ICipher c = new BlockTransposition(5);
			((BlockTransposition) c).PaddingValue = 'A';
			Evaluation eval = new BigramEvaluation(c, BigramEvaluation.SLOVAK_BIGRAMS);
			object[] text = {
				'S', 'T', 'A', 'T', 'I', 'S', 'T', 'I', 'K', 'A',
				'S', 'L', 'O', 'V', 'O', 'X', 'X', 'S', 'I', 'F',
				'R', 'A'
			};
			Permutation p = new Permutation(5, 40);
			IKey k = new PermutationKey(p);
			object[] closedText = c.Encrypt(text, k);
			eval.Evaluate(closedText, (Solution) p.Inverse());
		}

		[Test()]
		public void NGramStatisticTest() {
			object [] text = { 'A', 'B', 'C', 'D', 'E' };
			NgramStatistic statistic = new NgramStatistic(BigramEvaluation.SLOVAK_BIGRAMS);

			statistic.Compare(2, text);
		}

		[Test()]
		public void AgentTest() {
			InitStepDecorator sd = Metaheuristic.PermutationStep;
			Agent a = new Bee(sd(), Metaheuristic.FindPermutation);
			Agent b = new Bee(sd(), Metaheuristic.FindPermutation);
			Agent c = new Bee(sd(), Metaheuristic.FindPermutation);
			Agent d = new Bee(sd(), Metaheuristic.FindPermutation);

			a.Solution = new Permutation(4);
			b.Solution = new Permutation(4);
			c.Solution = new Permutation(4);
			d.Solution = new Permutation(4);

			a.Solution.Fitness = 2.0;
			b.Solution.Fitness = 1.0;
			c.Solution.Fitness = 0.0;
			d.Solution.Fitness = 3.0;

			Assert.AreEqual(true, a.IsBetter(b.Solution));
			Assert.AreEqual(false, !a.IsBetter(b.Solution));

			List<Agent> list = new List<Agent>();
			list.Add(a);
			list.Add(b);
			list.Add(c);
			list.Add(d);

			list.Sort();

			Assert.AreEqual(3.0, list[0].Solution.Fitness);
			Assert.AreEqual(0.0, list[3].Solution.Fitness);
		}

		[Test]
		public void TableTest() {
			//TableSpace ts = new TableSpace(Utils.STA, Utils.STA_FREQUENCIES, 100);
			TableStepProvider sp = new TableStepProvider();
			Table t1 = new Table(Utils.STA, Utils.STA_FREQUENCIES);
			Table t2 = new Table(Utils.STA, Utils.STA_FREQUENCIES);
			object[] values = new object[100];
			for (int i = 0; i < values.Length; i++) values[i] = i;
			t1.SetRandom(values);
			t2.SetRandom(values);

			List<Step> steps = sp.FindSteps(t1, t2);
			foreach(Step s in steps) {
				sp.DoStep(t1, s);
			}
			Assert.AreEqual(true, t1.Equals(t2));
		}

		[Test]
		public void HeuristicsRunTest() {
			StreamReader sr = new StreamReader("data/text/laktibrada-tsa-200.txt");
			char[] data = sr.ReadToEnd().ToCharArray();
			object[] openText = new object[data.Length];
			int i = 0;
			foreach(char c in data) openText[i++] = c;
			HeuristicFactory factory = new HeuristicFactory(Utils.STA);
			StreamWriter sw = new StreamWriter("run-test.txt");

			List<swarm.TestCase> cases = factory.ReadTestCases("data/testcase/testcase.xml");
			foreach(swarm.TestCase t in cases) {
				Metaheuristic[] heuristics = t.Metahueristics;
				foreach (Metaheuristic m in heuristics) {
                    m.Run(3);
					sw.Write(m.ToString());
				}
			}
			sw.Close();
		}
	}
}