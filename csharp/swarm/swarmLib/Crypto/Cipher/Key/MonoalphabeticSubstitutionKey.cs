using System;
using System.Collections;

namespace swarmLib {
	public class MonoalphabeticSubstitutionKey : IKey {
		private Hashtable _map;
		private Permutation _p;

		public MonoalphabeticSubstitutionKey(Permutation p) {
			int[] data = p.GetData();
			_p = p;
			_map = new Hashtable();
			for(int i = 0; i < data.Length; i++) {
				_map[(char) (i + 'A')] = (char) (data[i] + 'A');
			}
		}

		public char Substitute(char c) {
			if (!_map.ContainsKey(c)) {
				throw new Exception("There is not mapping for character: " + c);
			}
			return (char) _map[c];
		}

		public override string ToString (){
			return _p.ToString();
		}

		public int GetLength() {
			return _p.GetLength();
		}
	}
}

