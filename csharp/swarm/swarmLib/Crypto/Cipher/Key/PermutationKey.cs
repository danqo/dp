using System;

namespace swarmLib
{
	public class PermutationKey : IKey {
		Permutation _permutation;
		
		public Permutation Permutation {
			get { return _permutation; }
			set { _permutation = value; }
		}
		
		public PermutationKey (Permutation permutation) {
			_permutation = permutation;
		}

		public override string ToString() {
			return _permutation.ToString();
		}

		public int GetLength() {
			return _permutation.GetLength();
		}
	}
}

