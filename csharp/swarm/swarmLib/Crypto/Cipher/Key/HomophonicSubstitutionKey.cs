using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace swarmLib {
	public class HomophonicSubstitutionKey : IKey {
		private Hashtable _map;
		private Hashtable _reverse;
		private Permutation _permutation;

		public Table Table {
			set {
				foreach (object r in value.GetRows()) {
					List<object> row = value.GetRow(r);
					this.AddKey(r, row.ToArray());
				}
			}
		}

		public HomophonicSubstitutionKey() {
			_map = new Hashtable();
			_reverse = new Hashtable();
		}

		public HomophonicSubstitutionKey(XmlNode node) {
			_map = new Hashtable();
			_reverse = new Hashtable();
			foreach (XmlNode n in node.ChildNodes) {
				object key = Char.Parse(n.Attributes["char"].Value);
				foreach(string s in n.InnerText.Split(new char[] {','})) {
					this.AddKey(key, int.Parse(s));
				}
			}
		}

		public HomophonicSubstitutionKey(Permutation p, object[] alphabet, int[] frequencies) {
			_permutation = p;
			_map = new Hashtable();
			_reverse = new Hashtable();
			int[] data = p.GetData();
			int index = 0;
			for (int i = 0; i < alphabet.Length; i++) {
				object key = alphabet[i];
				for (int j = 0; j < frequencies[i]; j++) {
					this.AddKey(key, data[index++]);
				}
			}
		}

		public void AddKey(object key, object[] values) {
			foreach(object v in values) {
				this.AddKey(key, v);
			}
		}

		public void AddKey(object key, object val) {
			if (!_map.ContainsKey(key)) {
				_map.Add(key, new List<object>(50));
			}
			List<object> k = (List<object>) _map[key];
			k.Add(val);
			_reverse.Add(val, key);
		}

		public List<object> GetValues(object key) {
			if (!_map.ContainsKey(key)) {
				throw new Exception("There is not mapping for key: " + key);
			}
			return (List<object>) _map[key];
		}

		public object GetKeyFor(object value) {
			if (!_reverse.ContainsKey(value)) {
				throw new Exception("There in not key for value: " + value);
			}
			return (object) _reverse[value];
		}

		public override string ToString (){
			if(_permutation != null) {
				return _permutation.ToString();
			}
			return "Source permutation not defined";
		}

		public int GetLength() {
			return _permutation.GetLength();
		}
	}
}