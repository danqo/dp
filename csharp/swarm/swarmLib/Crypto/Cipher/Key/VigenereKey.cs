using System;
using System.Text;

namespace swarmLib {
	public class VigenereKey : IKey {

		private object[] _keyWord;


		public VigenereKey(object[] keyWord) {
			_keyWord = keyWord;
		}

		public VigenereKey(string keyWord) {
			int i = 0;
			_keyWord = new object[keyWord.Length];
			foreach(char c in keyWord) {
				_keyWord[i++] = c;
			}
		}

		public VigenereKey(Solution solution) {
			Vector keyWord = (Vector) solution;
			_keyWord = keyWord.Data;
		}

		public object[] KeyWord {
			get { return _keyWord; }
		}

		public override string ToString (){
			StringBuilder sr = new StringBuilder();
			foreach(char c in _keyWord) sr.Append(c);
			return sr.ToString();
		}

		public int GetLength() {
			return _keyWord.Length;
		}
	}
}

