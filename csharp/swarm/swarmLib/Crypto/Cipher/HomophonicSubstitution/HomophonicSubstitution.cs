using System;
using System.Collections;
using System.Collections.Generic;

namespace swarmLib {
	public class HomophonicSubstitution : ICipher {
		private int[] _frequencies;
		private object[] _alphabet;

		public HomophonicSubstitution() { }

		public HomophonicSubstitution(object[] alphabet, int[] frequencies) {
			_alphabet = alphabet;
			_frequencies = frequencies;
		}

		public object[] Encrypt (object[] data, IKey key) {
			HomophonicSubstitutionKey k = (HomophonicSubstitutionKey) key;
			Random r = new Random(DateTime.UtcNow.Millisecond);
			object[] result = new object[data.Length];
			int i = 0;

			foreach(object c in data) {
				List<object> values = k.GetValues((char) c);
				object e = values[r.Next() % values.Count];
				result[i++] = e;
			}
			return result;
		}

		public object[] Decrypt (object[] data, IKey key) {
			HomophonicSubstitutionKey k = (HomophonicSubstitutionKey) key;
			object[] result = new object[data.Length];
			int i = 0;

			foreach(object o in data) {
				object c = k.GetKeyFor(o);
				result[i++] = c;
			}
			return result;
		}

		public IKey CreateKey(Solution solution) {
			Permutation p = (Permutation) solution;
			HomophonicSubstitutionKey key = new HomophonicSubstitutionKey(p, _alphabet, _frequencies);
			return key;
			//Table t = (Table) solution;
			//HomophonicSubstitutionKey key = new HomophonicSubstitutionKey();
			//key.Table = t;
			//return key;
		}
	}
}
