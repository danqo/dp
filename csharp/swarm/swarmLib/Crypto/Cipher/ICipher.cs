using System;

namespace swarmLib
{
	public interface ICipher {
		object[] Encrypt(object[] data, IKey key);
		object[] Decrypt(object[] data, IKey key);
		IKey CreateKey(Solution solution);
	}
}