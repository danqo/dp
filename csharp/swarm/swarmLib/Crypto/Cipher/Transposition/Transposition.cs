using System;

namespace swarmLib
{
	public class Transposition : ICipher
	{
		public Transposition () {}

		public object[] Encrypt(object[] data, IKey key) {
			PermutationKey k = (PermutationKey) key;
			Permutation p = k.Permutation;
			return p.ApplyTo(data);
		}
	
		/*
		public string Encrypt(string data, IKey key) {
			PermutationKey k = (PermutationKey) key;
			Permutation p = k.Permutation;
			object[] e = p.ApplyTo(Utils.StringToObjectsArray(data));
			return new string(Utils.ObjectArrayToCharArray(e));
		}
		*/
	
		public object[] Decrypt(object[] data, IKey key)
		{
			PermutationKey k = (PermutationKey) key;
			Permutation p = k.Permutation;
			return p.ApplyTo(data);
		}
		
		/*
		public string Decrypt(string data, IKey key) {
			PermutationKey k = (PermutationKey) key;
			Permutation p = k.Permutation;
			object[] e = p.ApplyTo(Utils.StringToObjectsArray(data));
			return new string((char[])e);
		}*/

		public IKey CreateKey(Solution solution) {
			return new PermutationKey((Permutation) solution);
		}
	}
}