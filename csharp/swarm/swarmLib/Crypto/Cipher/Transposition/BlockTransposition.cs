using System;
using System.Linq;
using System.Text;

namespace swarmLib
{
	public class BlockTransposition : ICipher
	{
		int _blockLen;
		object _paddingValue = 'A';
		
		public int BlockLen {
			get {return _blockLen;}
			set {_blockLen = value;}
		}
		
		public object PaddingValue {
			get {return _paddingValue;}
			set {_paddingValue = value;}
		}
		
		public BlockTransposition (int blockLen) {
			_blockLen = blockLen;
		}
		
		private object[] TransposeBlocks(object[] data, IKey key) {
			PermutationKey k = (PermutationKey) key;
			Permutation p = k.Permutation;
			
			int offset = 0;
			int padLen = _blockLen - (data.Length % _blockLen);
			object[] paddedData = Enumerable.Repeat(_paddingValue, data.Length + padLen).ToArray();
			Array.Copy(data, paddedData, data.Length);
			
			object[] result = new object[paddedData.Length]; 
			while (offset < paddedData.Length) {
				object[] block = new object[_blockLen];
				Array.Copy(paddedData, offset, block, 0, block.Length);
				block = p.ApplyTo(block);
				Array.Copy(block, 0, result, offset, block.Length);
				offset += _blockLen;
			}
			return result;
		}
		
		public object[] Encrypt(object[] data, IKey key) {
			return TransposeBlocks(data, key);
		}

		//very slow
		public string Encrypt(string data, IKey key) {
			object[] data_obj = new object[data.Length];
			int i = 0;
			foreach(char c in data.ToCharArray()) {
				data_obj[i++] = c;
			}
			object[] result_obj = TransposeBlocks(data_obj, key);
			StringBuilder sb = new StringBuilder();
			foreach(object o in result_obj) {
				if (o == null) continue;
				sb.Append((char) o);
			}
			return sb.ToString();
		}
	
		public object[] Decrypt(object[] data, IKey key) {
			return TransposeBlocks(data, key);
		}

		//very slow
		public string Decrypt(string data, IKey key) {
			object[] data_obj = new object[data.Length];
			int i = 0;
			foreach(char c in data.ToCharArray()) {
				data_obj[i++] = c;
			}
			object[] result_obj = TransposeBlocks(data_obj, key);
			StringBuilder sb = new StringBuilder();
			foreach(object o in result_obj) {
				sb.Append((char) o);
			}
			return sb.ToString();
		}

		public IKey CreateKey(Solution solution) {
			return new PermutationKey((Permutation) solution);
		}
	}
}

