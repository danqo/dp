using System;

namespace swarmLib {
	public class Vigenere : ICipher {

		public Vigenere () {

		}

		private object[] process(object[] data, IKey key, bool encryption) {
			VigenereKey k = (VigenereKey) key;
			int i = 0;
			int j = 0;
			object[] keyWord = k.KeyWord;
			object[] result = new object[data.Length];
			foreach(object o in data) {
				char c = (char) o;
				char r;
				if (encryption) {
					r =  (char) ((c - 'A') + ((char)keyWord[i] - 'A'));
				} else {
					int t =  (int) ((c - 'A') - ((char)keyWord[i] - 'A'));
					t = (t < 0)? (t + 26) : t;
					r = (char) t;
   				}
				result[j++] = (char) ((r % 26) + 'A');
				i = (i + 1) % keyWord.Length;
			}
			return result;
		}

		#region ICipher implementation
		public object[] Encrypt (object[] data, IKey key) {
			return process(data, key, true);
		}

		public object[] Decrypt (object[] data, IKey key) {
			return process(data, key, false);
		}

		public IKey CreateKey(Solution solution) {
			return new VigenereKey((Vector) solution);
		}
		#endregion
	}
}

