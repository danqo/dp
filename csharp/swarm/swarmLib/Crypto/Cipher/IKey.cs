using System;

namespace swarmLib
{
	public interface IKey {
		string ToString();
		int GetLength();
	}
}