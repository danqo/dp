using System;
using System.Collections;

namespace swarmLib {
	public class MonoalphabeticSubstitution : ICipher {
		public MonoalphabeticSubstitution () {

		}

		#region ICipher implementation
		public object[] Encrypt (object[] data, IKey key) {
			MonoalphabeticSubstitutionKey k = (MonoalphabeticSubstitutionKey) key;
			object[] result = new object[data.Length];
			int i = 0;
			foreach(object o in data) {
				result[i++] = k.Substitute((char) o);
			}
			return result;
		}

		public object[] Decrypt (object[] data, IKey key) {
			MonoalphabeticSubstitutionKey k = (MonoalphabeticSubstitutionKey) key;
			object[] result = new object[data.Length];
			int i = 0;
			foreach(object o in data) {
				result[i++] = k.Substitute((char) o);
			}
			return result;
		}

		public IKey CreateKey(Solution solution) {
			return new MonoalphabeticSubstitutionKey((Permutation) solution);
		}
		#endregion
	}
}