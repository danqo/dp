using System;
using System.Collections;
using System.Text;
using System.IO;

namespace swarmLib {
	public class BigramEvaluation : Evaluation {
		public static string SLOVAK_BIGRAMS = "data/slovak_bigrams.txt";
		private double[,] _ref;

		public BigramEvaluation(ICipher cipher, string path) {
			_cipher = cipher;
			_ref = new double[26, 26];
			char[] separator = new char[1] {','};
			using(StreamReader sr = new StreamReader(path)) {
				string line;
				while ((line = sr.ReadLine()) != null) {
					string[] pair = line.Split(separator);
					double val = double.Parse((string) pair[1]);
					int i = pair[0][0] - 'A';
					int j = pair[0][1] - 'A';
					_ref[i, j] = val / 1000;
				}
			}
		}

		private double[,] FindBigrams(object[] text) {
			double[,] measured = new double[26,26];
			for (int i = 0; i < text.Length - 1; i++) {
				measured[ (char) text[i] - 'A', (char) text[i + 1] - 'A'] += 1;
			}
			for (int i = 0; i < 26; i++) {
				for (int j = 0; j < 26; j++) {
					measured[i, j] = measured[i, j] / text.Length;
				}
			}
			return measured;
		}

		private double Compare(double[, ] referece, double[, ] measured) {
			double diff = 0;
			for (int i = 0; i < 26; i++) {
				for (int j = 0; j < 26; j++) {
					double d = referece[i, j] - measured[i,j];
					d = (d < 0)? d * (-1): d;
					diff += d;
				}
			}
			return diff;
		}

		public override double Evaluate(object[] text, Solution solution) {
			IKey key = _cipher.CreateKey(solution);
			return this.Evaluate(text, key);
		}

		public override double Evaluate(object[] text, IKey key) {
			object[] decryptedText = _cipher.Decrypt(text, key);
			return 1 / Compare(_ref, FindBigrams(decryptedText));
		}
	}
}

