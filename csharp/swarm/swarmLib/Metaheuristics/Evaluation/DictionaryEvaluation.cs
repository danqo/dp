using System;
using System.IO;

namespace swarmLib {
	public class DictionaryEvaluation : Evaluation {
		private Tree _tree;
		private int _minWordLen;

		public DictionaryEvaluation(ICipher c, string path, int minWordLen) {
			_cipher = c;
			_tree = new Tree(path);
			_tree.CreateTree();
			_minWordLen = minWordLen;
		}

		public override double Evaluate(object[] text, Solution solution) {
			IKey key = _cipher.CreateKey(solution);
			return this.Evaluate(text, key);
		}

		public override double Evaluate(object[] text, IKey key) {
			object[] decryptedText = _cipher.Decrypt(text, key);
			return _tree.Analyze(decryptedText, _minWordLen);
		}
	}

	class Tree {
        Atom _root;
        public string _dictPath;
        public bool _isCreated {get; set;}

		public Tree() {
            _root = new Atom();
            _root.Word = " ";
            this.CreateTree();
        }

		public Tree(string dictPath) {
            this._dictPath = dictPath;
            _root = new Atom();
            _root.Word = " ";
            this.CreateTree();
        }

		public string DictionaryPath {
            get { return _dictPath; }
            set {
                this._dictPath = value;
                _root = new Atom();
                _root.Word = " ";
                this.CreateTree();
            }
        }

        public void InsertWord(String w) {
            int i = 0;
            char[] word = w.ToCharArray();
            Atom p = _root;

            for (i = 0; i < word.Length; i++) {
                if (p.GetNext((int)word[i] - (int)'A') == null) {
                    p.SetNext((int)word[i] - (int)'A');
                    p = p.GetNext((int)word[i] - (int)'A');
                }
                else {
                    p = p.GetNext((int)word[i] - (int)'A');
                }
            }
            p.IsWord = true;
        }

        public void CreateTree() {
            StreamReader sr;
            _isCreated = true;
            try {
                sr = new StreamReader(_dictPath);
                while (!sr.EndOfStream) {
                    InsertWord(sr.ReadLine());
                }
                sr.Close();
            }
            catch(IOException e) {
				Console.WriteLine(e.Message);
                _isCreated = false;
            }
        }

        public int SearchLongest(object[] w, int start) {
            int length = 0;
            Atom p = _root;
            for (int i = start; i < w.Length; i++) {
				int c =  Convert.ToInt32(w[i]);
				int A = (int) 'A';
				Atom next = p.GetNext(c - A);
                if (next != null) {
                    p = next;
                    if (p.IsWord == true) {
                        length = i - start;
					}
                } else {
                    break;
                }
            }
            return length + 1;
        }

        public double Analyze(object[] openText, int minWordLen) {
            int wordLen = 0, pos = 0, price = 0;
            int longCount = 0;
            while(pos < openText.Length) {
                wordLen = this.SearchLongest(openText, pos);
                if(wordLen >= minWordLen) {
                    if (wordLen > minWordLen + 1) {
                        price += 20;
                        longCount++;
                    }
                    price += (wordLen * wordLen);
                }
                if (wordLen == 0) {
                    wordLen = 1;
                }
                pos += wordLen;
            }
            return Convert.ToDouble(price);
        }

        internal Atom Atom {
            get {
                throw new System.NotImplementedException();
            }
            set {
            }
        }
    }

	class Atom {
        String word;
        bool is_word;
        Atom[] next = new Atom[Utils.STA.Length];

        public Atom() {
            this.IsWord = false;
        }

        public string Word {
            get { return word; }
            set { this.word = value; }
        }

        public Atom GetNext(int i) {
            return next[i];
        }

        public void SetNext(int i) {
            this.next[i] = new Atom();
        }

        public bool IsWord {
            get { return this.is_word; }
            set { this.is_word = value; }
        }
    }
}