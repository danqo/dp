using System;

namespace swarmLib {
	public abstract class Evaluation {
		protected ICipher _cipher;

		public ICipher Cipher {
			get { return _cipher; }
		}

		public Evaluation () {}
		public abstract double Evaluate(object[] text, Solution solution);
		public abstract double Evaluate(object[] text, IKey key);
	}
}

