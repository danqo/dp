using System;
using System.Collections.Generic;

namespace swarmLib {
	public class MonogramEvaluation : Evaluation {
		private NgramStatistic _statistic;
		private int _partition = 1;

		public int Partition {
			get { return _partition; }
			set { _partition = value; }
		}

		public MonogramEvaluation (ICipher cipher, string path) {
			_cipher = cipher;
			_statistic = new NgramStatistic(path);
		}

		public override double Evaluate(object[] text, Solution solution) {
			IKey key = _cipher.CreateKey(solution);
			return this.Evaluate(text, key);
		}

		public override double Evaluate(object[] text, IKey key) {
			int monograms = 1;
			object[] decryptedText = _cipher.Decrypt(text, key);
			if (this.Partition == 1) {
				return 1.0 / _statistic.Compare(monograms, decryptedText);
			} else {
				object[] partitions = new object[this.Partition];
				for (int i= 0; i < this.Partition; i++) {
					partitions[i] = new List<object>(decryptedText.Length/this.Partition);
				}
				int j = 0;
				foreach(Object o in decryptedText) {
					((List<object>) partitions[j]).Add(o);
					j = (j + 1) % this.Partition;
				}

				double sum = 0.0;
				foreach(List<object> l in partitions) {
					sum += _statistic.Compare(monograms, l.ToArray());
				}
				return 1.0 / sum;
			}
		}
	}
}

