using System;
using System.Xml;
using swarmLib;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using ZedGraph;
using System.Drawing;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Imaging;


namespace swarm {
	public class TestCase {
		private static Color[] colors = new Color[] {Color.Red, Color.Blue, Color.Green, Color.Orange, Color.Pink, Color.DarkMagenta};
		private string _name;
		private string _reportDir;
		private int _repeat;
		private Hashtable _heuristicsInfo;
		private Metaheuristic[] _heuristics;
		private Hashtable _closedTexts;
		private IKey _key;
		private Solution _solution;

		public Metaheuristic[] Metahueristics {
			get { return _heuristics; }
		}

		public TestCase(
			string name,
			string reportDir,
			Hashtable heuristicsInfo,
			Metaheuristic[] heuristics,
			int repeat,
			Hashtable closedTexts,
			IKey key,
			Solution solution
		) {
			_name = name;
			_reportDir = reportDir;
			_repeat = repeat;
			_closedTexts = closedTexts;
			_key = key;
			_heuristicsInfo = heuristicsInfo;
			_heuristics = heuristics;
			_solution = solution;
		}

		public Hashtable Run() {
			Hashtable data = new Hashtable();

			foreach (DictionaryEntry ct in _closedTexts) {
				Hashtable textData = new Hashtable();
				data.Add(ct.Key, textData);

				foreach (Metaheuristic m in _heuristics) {
					Hashtable heuristicData = new Hashtable();
					textData.Add(m.Name, heuristicData);
					Console.WriteLine("{0}: {1}", _name, m.Name);
					Hashtable info = (Hashtable) _heuristicsInfo[m.Name];
					int agentCount = (int) info["agents"];
					int iterations = (int) info["iterations"];

					List<double> progress = new List<double>();
					List<Solution> solutions = new List<Solution>();
					object[] closedText = (object[]) ct.Value;
					for (int i = 0; i < _repeat; i++) {
						m.Initialize(agentCount, closedText, _key);
						m.Run(iterations);
						// sum progress in repetions
						for(int j = 0; j < m.Progress.Count;j++) {
							if (progress.Count <= j) {
								progress.Add(m.Progress[j]);
							} else {
								progress[j] += m.Progress[j];
							}
						}
						// Save founded solutions
						solutions.AddRange(m.Best);
					}
					//save average progress for heuristic
					for (int j = 0; j < m.Progress.Count;j++) {
						progress[j] /= _repeat;
					}
					heuristicData.Add("avg-progress", new List<double>(progress));
					heuristicData.Add("operations", new List<double>(m.Operations));
					heuristicData.Add("length", closedText.Length);
					heuristicData.Add("solutions", solutions);
					heuristicData.Add("solution", _solution);
					heuristicData.Add("agentCount", agentCount);
					heuristicData.Add("space", m.Space);
				}
			}
			return data;
		}

		private void AddChart(string name, double[] x, double[] y, Color color, GraphPane pane, double[] minMax) {
			PointPairList points = new PointPairList();
			double min = Double.MaxValue;
			double max = Double.MinValue;
			for (int i = 0; i < x.Length; i++) {
				if (i > 0 && y[i] < y[i-1]) {
					break;
				}
				points.Add(x[i], y[i]);
				if (y[i] < min) min = y[i];
				if (y[i] > max) max = y[i];
			}

			if (min < minMax[0]) minMax[0] = min;
			if (max > minMax[1]) minMax[1] = max;
			pane.AddCurve(name, points, color, SymbolType.None);
			pane.XAxis.Scale.FormatAuto = true;
			pane.AxisChange();
		}

		private void TextReport(string heuristicName, List<Solution> solutions, StringBuilder sb) {
			sb.AppendFormat("Heuristic: {0}\n", heuristicName);
			solutions.Sort();
			foreach(Solution s in solutions) {
				sb.AppendLine(s.ToString());
			}
			sb.AppendLine("KEY: " + _key.ToString() +"\n\n");
		}

		public void Report(Hashtable data) {
			if(!Directory.Exists(_reportDir)) {
				Directory.CreateDirectory(_reportDir);
			}

			StringBuilder sb = new StringBuilder();
			foreach (DictionaryEntry i in data) {
				ZedGraph.GraphPane pane = new ZedGraph.GraphPane();
				List<Color> colorStack = new List<Color>();
				colorStack.AddRange((Color[])colors.Clone());
				double[] minMax = new double[] {Double.MaxValue, Double.MinValue};
				int textLength = 0;
				string textName = (string) i.Key;
				Hashtable textData = (Hashtable) i.Value;
				sb.AppendFormat("Text: {0}\n", textName);
				foreach (DictionaryEntry j in textData) {
					string heuristicName = (string) j.Key;
					Hashtable heuristicData = (Hashtable) j.Value;

					List<double> progress = (List<double>) heuristicData["avg-progress"];
					List<double> operations = (List<double>) heuristicData["operations"];
					textLength = (int) heuristicData["length"];
					List<Solution> solutions = (List<Solution>) heuristicData["solutions"];

					TextReport(heuristicName, solutions, sb);
					AddChart(heuristicName, operations.ToArray(), progress.ToArray(), colorStack[0], pane, minMax);
					colorStack.RemoveAt(0);
				}

				pane.XAxis.Title.Text = "Evaluations";
				pane.YAxis.Title.Text = "Score";
				pane.Legend.FontSpec.Size = 16;
				pane.YAxis.Scale.Min = minMax[0] * 0.95;
				pane.Title.Text = _heuristics[0].CipherName + " (text length " + textLength + ", key length "+ _key.GetLength() + ")";
				Bitmap image = pane.GetImage(450, 450, 100, true);
				image.Save(_reportDir + _name + "-" + textName + "-progress" + ".png", ImageFormat.Png);
				sb.AppendLine("---------------------------------------------------------------\n");
			}

			StreamWriter sw = new StreamWriter(_reportDir + _name + ".txt");
			sw.Write(sb.ToString());
			sw.Close();

			/*
			//Add key
			{
				PointPairList key = new PointPairList();
				for (int j = 0; j < _progress[0].Length; j++) {
					key.Add(Convert.ToDouble(j), _heuristics[0].KeyFitness);
				}
				pane.AddCurve("Key", key, Color.Black, SymbolType.None);
				pane.YAxis.Scale.Max = _heuristics[0].KeyFitness * 1.05;
				pane.AxisChange();
			}
			// Operations graphs
			{
				double[] minMax = new double[] {Double.MaxValue, Double.MinValue};
				List<Color> colorStack = new List<Color>();
				colorStack.AddRange((Color[])colors.Clone());
				ZedGraph.GraphPane pane = new ZedGraph.GraphPane();
				foreach(Metaheuristic h in _heuristics) {
					AddChart(h.Name, h.Operations.ToArray(), colorStack[0], pane, minMax);
					colorStack.RemoveAt(0);
				}
				pane.XAxis.Title.Text = "Iterations";
				pane.YAxis.Title.Text = "Operations";
				pane.YAxis.Scale.MaxAuto = true;
				pane.YAxis.Scale.Min = 0.0;
				pane.AxisChange();
				pane.Title.Text = _heuristics[0].CipherName + " (text length " +  + ", key length "+ _key.GetLength() + ")";
				Bitmap image = pane.GetImage(1024, 1024, 180, true);
				image.Save(_reportDir + _name + "-operations" + ".bmp");
			}*/
		}

		public void SaveData(Hashtable data, string path) {
			path = (path == null)? _reportDir + _name + "-data" + ".data": path;
			if (File.Exists(path)) File.Delete(path);
			FileStream stream = File.Create(path);
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(stream, data);
			return;
		}

		public Hashtable LoadData(string path) {
			path = (path == null)? _reportDir + _name + "-data" + ".data": path;
			FileStream stream = File.OpenRead(path);
			BinaryFormatter formatter = new BinaryFormatter();
			Hashtable data = (Hashtable) formatter.Deserialize(stream);
			stream.Close();
			return data;
		}


		private static Hashtable ParseTexts(XmlNode node) {
			Hashtable texts = new Hashtable();
			foreach(XmlNode n in node.ChildNodes) {
				texts.Add(n.InnerText, Utils.ReadFile("data/text/" + n.InnerText + ".txt"));
			}
			return texts;
		}

		private static Hashtable ParseHeuristics(XmlNode node) {
			Hashtable heuristics = new Hashtable();
			foreach(XmlNode n in node.ChildNodes) {
				Hashtable info = new Hashtable();
				info.Add("iterations", int.Parse(n.Attributes["iterations"].Value));
				info.Add("agents", int.Parse(n.Attributes["agents"].Value));
				heuristics.Add(n.InnerText, info);
			}
			return heuristics;
		}

		public static TestCase Parse(XmlNode node, HeuristicFactory factory) {
			XmlNodeList list = node.ChildNodes;
			int repeat = Int32.Parse(node.Attributes["repeat"].Value);
			// Texts
			Hashtable closedTexts = new Hashtable();
			Hashtable texts = ParseTexts(list.Item(0));

			//Heuristics info
			Hashtable heuristicsInfo = ParseHeuristics(list.Item(1));


			string keyXml = list.Item(3).InnerText;
			Hashtable heuristics = new Hashtable();
			ICipher cipher;
			Permutation p;
			Solution solution;
			IKey key;
			IKey dKey;
			SolutionSpace space;
			switch(list.Item(2).InnerText) {
			case "monoalphabetic-substitution":
				cipher = new MonoalphabeticSubstitution();
				p = new Permutation(keyXml);
				key = new MonoalphabeticSubstitutionKey(p);
				dKey = new MonoalphabeticSubstitutionKey(p.Inverse());
				solution = p.Inverse();
				heuristics = factory.SubstitutionHeuristics(Evals.BigramStat);
				break;
			case "block-transposition":
				p = new Permutation(keyXml);
				int blockLen = p.GetLength();
				key = new PermutationKey(p);
				dKey = new PermutationKey(p.Inverse());
				solution = p.Inverse();
				cipher = new BlockTransposition(blockLen);
				heuristics = factory.TranspositionHeuristics(blockLen, Evals.Dictionary);
				break;
			case "vigenere":
			    cipher = new Vigenere();
				key = new VigenereKey(keyXml);
				solution = new Vector(keyXml);
				dKey = key;
				space = new VectorSpace(keyXml.Length, Utils.STA);
				heuristics = factory.VigenereHeuristics((VectorSpace) space);
				break;
			case "homophonic-substitution":
				p = new Permutation(keyXml);
				solution = p;
				int i = 0;
				string[] freq = list.Item(2).Attributes["frequencies"].Value.Split(new char[] {','});
				int[] frequencies = new int[freq.Length];
				foreach(string s in freq){
					frequencies[i++] = Int32.Parse(s);
				}
				cipher = new HomophonicSubstitution(Utils.STA, frequencies);
				key = new HomophonicSubstitutionKey(p, Utils.STA, frequencies);
				dKey = key;
				space = new PermutationSpace(p.GetLength());
				heuristics = factory.HomophonicSubstitutionHeuristics((PermutationSpace) space, Evals.BigramStat, frequencies);
				break;
			default:
				throw new Exception("Unknown cipher: " + list.Item(2).InnerText);
			}

			foreach (DictionaryEntry i in texts) {
				object[] text = (object[]) i.Value;
				closedTexts.Add(i.Key, cipher.Encrypt(text, key));
			}

			List<Metaheuristic> hs = new List<Metaheuristic>();
			foreach (DictionaryEntry j in heuristics) {
				string name = (string) j.Key;
				Metaheuristic h = (Metaheuristic) j.Value;
				if (heuristicsInfo.ContainsKey(name)) {
					hs.Add(h);
				}
			}

			return new TestCase(
				node.Attributes["name"].Value,
				node.Attributes["reportDir"].Value,
				heuristicsInfo,
				hs.ToArray(),
				repeat,
				closedTexts,
				dKey,
				solution
			);
		}
	}
}