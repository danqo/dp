using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;

namespace swarmLib {
	[Serializable()]
	public class TableSpace : SolutionSpace {
		private object[] _alphabet;
		private int[] _frequencies;

		public object[] Alphabet {
			get { return _alphabet; }
		}

		public int[] Frequencies {
			get { return _frequencies; }
		}

		public TableSpace(object[] alphabet, int[] frequencies, int tableSize) {
			_dimension = tableSize;
			_elements = new object[tableSize];
			_frequencies = frequencies;
			_alphabet = alphabet;
			for (int i = 0; i < tableSize; i++) {
				_elements[i] = i;
			}
		}

		public TableSpace(object[] alphabet, XmlNode node) {
			int dimension = 0;
			_alphabet = alphabet;
			List<int> frequencies = new List<int>();
			List<object> values = new List<object>();

			Hashtable map = new Hashtable();
			foreach (XmlNode n in node.ChildNodes) {
				object key = Char.Parse(n.Attributes["char"].Value);
				List<object> vals = new List<object>();
				foreach(string o in n.InnerText.Split(new char[] {','})) {
					vals.Add(int.Parse(o));
					values.Add(int.Parse(o));
				}
				map.Add(key, vals.ToArray());

			}

			foreach (char c in alphabet) {
				if (!map.ContainsKey(c)) {
                   throw new Exception("Unmapped character: " + c);
				}
				frequencies.Add(((object[]) map[c]).Length);
				dimension += ((object[]) map[c]).Length;
			}
			_frequencies = frequencies.ToArray();
			_dimension = dimension;
			values.Sort();
			_elements = values.ToArray();
		}

		public override int GetSize() {
			throw new NotImplementedException();
		}
	}
	[Serializable()]
	public class TableStepProvider : StepProvider {
		public override void DoRandomStep(Solution solution, object[] possibilities) {
			Random r = new Random(DateTime.UtcNow.Millisecond);
			Table t = (Table) solution;
			object[] rows = new object[t.GetRows().Count];
			t.GetRows().CopyTo(rows, 0);

			// Find random coords
			object[] coordsA = new object[2];
			object[] coordsB = new object[2];
			coordsA[0] = rows[r.Next(0, rows.Length - 1)];
			coordsB[0] = rows[r.Next(0, rows.Length - 1)];
			List<object> r1 = t.GetRow(coordsA[0]);
			List<object> r2 = t.GetRow(coordsB[0]);
			coordsA[1] = r.Next(0, r1.Count -1);
			coordsB[1] = r.Next(0, r2.Count -1);

			// Swap coords
			t.SwapComponents(coordsA, coordsB);
		}

		public override void DoStep(Solution solution, Step step) {
			TableStep tc = (TableStep) step;
			object[] coords = ((Table) solution).GetCoords(tc.Value);
			solution.SwapComponents(tc.Coords, coords);
		}

		public override List<Step> FindSteps(Solution a, Solution b) {
			Table tA = (Table) a;
			Table tB = (Table) b;
			List<Step> steps = new List<Step>();
			foreach (object r in tA.GetRows()) {
				List<object> rowA = tA.GetRow(r);
				List<object> rowB = tB.GetRow(r);
				for (int i = 0; i < rowA.Count; i++) {
					// TODO: fix swaps in same rows 
					//if(rowB.Contains(rowA[i])) continue;
					if (rowA[i].Equals(rowB[i])) continue;
					object[] source = new object[2] {r, i};
					object dest = rowB[i];
					TableStep step = new TableStep(source, dest);
					steps.Add(step);
				}
			}
			return steps;
		}

		public override double[] UpdateSpeed(Agent a, double[] speed, Solution gbest, double w, double c1, double c2) {
			Particle p = (Particle) a;
			Table solution = (Table) p.Solution;
			Table pbest = (Table) p.PBest;
			if (!p.IsBetter(gbest) && p.Solution.Fitness != gbest.Fitness) {
				for (int i = 0; i < speed.Length; i++) {
					double present = Convert.ToDouble((int) solution.GetComponentWithIndex((int)i));
					double pb = Convert.ToDouble(pbest.GetComponentWithIndex(i));
					double gb = Convert.ToDouble(((Table) gbest).GetComponentWithIndex(i));
					speed[i] = w * speed[i] + c1 * (pb - present) + c2 * (gb- present);
				}
			} else {
				for (int i = 0; i < speed.Length; i++) {
					double present = Convert.ToDouble(solution.GetComponentWithIndex(i));
					double pb = Convert.ToDouble(pbest.GetComponentWithIndex(i));
					speed[i] = w * speed[i] + c1 *  (pb - present);
				}
			}
			return speed;
		}

		public override Solution UpdatePosition(SolutionSpace space, Solution solution, Solution gbest, Solution pbest, double[] speed) {
			double maxSpeed = speed[Utils.GetMax(speed)];
			Random r = new Random(DateTime.UtcNow.Millisecond);


			for (int i = 0; i < speed.Length; i++) {
				double normalized = speed[i] / maxSpeed;
				normalized = (normalized > 0.0) ? normalized : normalized * (-1.0);
				if (normalized >= r.NextDouble()) {
					object[] a = ((Table) solution).GetCoords(i);
					object[] b = ((Table) pbest).GetCoords(i);
					solution.SwapComponents(a, b);
				}
			}
			return solution;
		}

		public override Solution UpdatePosition(SolutionSpace space, Solution a, Solution b, double alpha, double beta, double gama) {
			throw new NotImplementedException();
		}
	}
	[Serializable()]
	class TableStep: Step {
		object[] _coords;
		object _value;

		public object[] Coords { get {return _coords;} }
		public object Value { get {return _value;} }

		public TableStep(object[] coordsA, object val) {
			_coords = coordsA;
			_value = val;
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("Coords: {0} {1} val: {2}", _coords[0], _coords[1], _value);
			return sb.ToString();
		}
	}
}