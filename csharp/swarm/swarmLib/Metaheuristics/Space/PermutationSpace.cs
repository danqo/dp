using System;
using System.Collections.Generic;

namespace swarmLib {
	[Serializable()]
	public class PermutationSpace : SolutionSpace {
		public PermutationSpace (int n) {
			_elements = new object[n];
			for(int i = 0; i< n; i++) _elements[i] = i;
			_dimension = n;
		}

		public override int GetSize() {
			return Utils.Factorial(_dimension);
		}

		public override List<Solution> GetSolutions() {
			List<Solution> r = new List<Solution>();
			Permutation p = new Permutation(_dimension, 0);
			while ((p = p.Successor()) != null) {
				r.Add((Solution) p.Clone());
			}
			return r;
		}
	}
	[Serializable()]
	class PermutationStepProvider : StepProvider {
		Random _random;

		public PermutationStepProvider() {
			_random = new Random(DateTime.Now.Millisecond);
		}

		public override void DoRandomStep(Solution solution, object[] possibilities) {
			int length = solution.GetLength();
			int a = _random.Next() % length;
			int b = _random.Next() % length;
			b = (a != b)? b : (b + 1) % length;
			solution.SwapComponents(a, b);
		}

		public override void DoStep(Solution solution, Step step) {
			int a = ((PermutationStep) step).A;
			int b = ((PermutationStep) step).B;
			solution.SwapComponents(a, b);
		}

		public override List<Step> FindSteps(Solution a, Solution b) {
			Permutation ap = (Permutation) a;
			Permutation bp = (Permutation) b;
			List<Step> steps = new List<Step>();
			for (int i = 0; i < ap.GetLength(); i++) {
				int ca = (int) ap.GetComponent(i);
				int cb = (int) bp.GetComponent(i);
				if (ca == cb) continue;
				steps.Add(new PermutationStep(i, (int) (bp.GetIndex(ca))[0]));
			}
			return steps;
		}

		public override double[] UpdateSpeed(Agent a, double[] speed, Solution gbest, double w, double c1, double c2) {
			Particle p = (Particle) a;
			Solution solution = p.Solution;
			Solution pbest = p.PBest;
			if (!p.IsBetter(gbest) && p.Solution.Fitness != gbest.Fitness) {
				for (int i = 0; i < speed.Length; i++) {
					double present = Convert.ToDouble((int) solution.GetComponent(i));
					double pb = Convert.ToDouble((int)pbest.GetComponent(i));
					double gb = Convert.ToDouble((int)gbest.GetComponent(i));
					speed[i] = w * speed[i] + c1 * (pb - present) + c2 * (gb- present);
				}
			} else {
				for (int i = 0; i < speed.Length; i++) {
					double present = Convert.ToDouble(solution.GetComponent(i));
					double pb = Convert.ToDouble((int) pbest.GetComponent(i));
					speed[i] = w * speed[i] + c1 * (pb - present);
				}
			}
			return speed;
		}

		public override Solution UpdatePosition(SolutionSpace space, Solution solution, Solution gbest, Solution pbest, double[] speed) {
			int swaps = _random.Next(1, space.Dimension / 2);
			for (int k = 0; k < swaps; k++) {
				if(_random.Next() % 2 == 0) {
					int i = Utils.RouleteSelection(speed);
					int gbestValue = (int) gbest.GetComponent(i);
					int index = (int) (solution.GetIndex(gbestValue)[0]);
					solution.SwapComponents(i, index);
				} else {
					int i = Utils.RouleteSelection(speed);
					int pbestValue = (int) pbest.GetComponent(i);
					int index = (int) (solution.GetIndex(pbestValue)[0]);
					solution.SwapComponents(i, index);
				}
			}
			return solution;
		}

		public override Solution UpdatePosition(SolutionSpace space, Solution a, Solution b, double alpha, double beta, double gama) {
			int swaps = _random.Next(1, space.Dimension / 2);
			for (int k = 0; k < swaps; k++) {
				int i = _random.Next(0, space.Dimension - 1);
				int val = (int) b.GetComponent(i);
				int index = (int) (a.GetIndex(val)[0]);
				a.SwapComponents(i, index);
			}
			return a;
		}
	}
	[Serializable()]
	class PermutationStep: Step {
		int a;
		int b;
		public int A { get {return a;} }
		public int B { get {return b;} }
		public PermutationStep(int a, int b) {
			this.a = a;
			this.b = b;
		}
	}
}

