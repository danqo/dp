using System;
using System.Collections.Generic;

namespace swarmLib {
	[Serializable()]
	public class VectorSpace : SolutionSpace {
		public VectorSpace(int dimension, object[] elements) {
			_dimension = dimension;
			_elements = elements;
		}

		public override int GetSize() {
			return (int) Math.Pow(_elements.Length, _dimension);
		}

		public override List<Solution> GetSolutions() {
			return new List<Solution>();
		}
	}
	[Serializable()]
	class VectorStepProvider : StepProvider {
		Random _random;
		public VectorStepProvider() {
			_random = new Random(DateTime.UtcNow.Millisecond);
		}
		public override void DoRandomStep(Solution solution, object[] possibilities) {
			int length = solution.GetLength();
			int position = _random.Next() % length;
			object val = possibilities[_random.Next() % possibilities.Length];
			solution.SetComponent(position, val);
		}

		public override void DoStep(Solution solution, Step step) {
			VectorStep vc = (VectorStep) step;
			((Vector) solution).SetComponent(vc.Position, vc.Value);
		}

		public override List<Step> FindSteps(Solution a, Solution b) {
			Vector vA = (Vector) a;
			Vector vB = (Vector) b;
			List<Step> steps = new List<Step>();
			for(int i = 0; i < vA.GetLength(); i++) {
				object ca = vA.GetComponent(i);
				object cb = vB.GetComponent(i);
				if (!ca.Equals(cb)) {
					steps.Add(new VectorStep(i, cb));
				}
			}
			return steps;
		}

		public override double[] UpdateSpeed(Agent a, double[] speed, Solution gbest, double w, double c1, double c2) {
			Particle p = (Particle) a;
			Solution solution = p.Solution;
			Solution pbest = p.PBest;
			if (!p.IsBetter(gbest) && p.Solution.Fitness != gbest.Fitness) {
				for (int i = 0; i < speed.Length; i++) {
					double present = Convert.ToDouble((char) solution.GetComponent(i) - 'A');
					double pb = Convert.ToDouble((char) pbest.GetComponent(i) - 'A');
					double gb = Convert.ToDouble((char) gbest.GetComponent(i) - 'A');
					speed[i] = w * speed[i] + c1 * (pb - present) + c2 * (gb- present);
				}
			} else {
				for (int i = 0; i < speed.Length; i++) {
					double present = Convert.ToDouble((char)solution.GetComponent(i) - 'A');
					double pb = Convert.ToDouble((char) pbest.GetComponent(i) - 'A');
					speed[i] = w * speed[i] + c1 * (pb - present);
				}
			}
			return speed;
		}

		public override Solution UpdatePosition(SolutionSpace space, Solution solution, Solution gbest, Solution pbest, double[] speed) {
			Vector s = (Vector) solution;
			for (int i = 0; i < speed.Length; i++) {
				// 25% probability to skip - improve convergation
				if (_random.Next() % 4 == 0) {
					continue;
				}
				int val = (int) ((char) s.GetComponent(i) - 'A');
			    int c = Convert.ToInt16(speed[i]);
				val = (val + c);
				val = (val % space.Elements.Length);
			    val = (val >= 0)? val: val + space.Elements.Length;
				val = (char) val + 'A';
				s.SetComponent(i, (char) val);
			}
			return solution;
		}

		public override Solution UpdatePosition(SolutionSpace space, Solution a, Solution b, double alpha, double beta, double gama) {
			Vector va = (Vector) a;
			Vector vb = (Vector) b;
			Vector result = new Vector(space.Dimension);
			for (int i = 0; i < space.Dimension; i++) {
				// 25% probability to skip - improve convergation
				if (_random.Next() % 4 == 0) {
					result.SetComponent(i, va.GetComponent(i));
					continue;
				}
				int valA = (int) ((char) va.GetComponent(i) - 'A');
				int valB = (int) ((char) vb.GetComponent(i) - 'A');
				int val = Convert.ToInt16(valA + (beta * (Math.Pow(Math.E, gama)) * (valA - valB)) + alpha);
				val = (val % space.Elements.Length);
			    val = (val >= 0)? val: val + space.Elements.Length;
				val = (char) val + 'A';
				result.SetComponent(i, (char) val);
			}
			return result;
		}
	}
	[Serializable()]
	class VectorStep: Step {
		int position;
		object val;
		public int Position { get {return position;} }
		public object Value { get {return val;} }

		public VectorStep(int position, object val) {
			this.position = position;
			this.val = val;
		}
	}
}

