using System;
using System.Collections.Generic;
using System.Collections;

namespace swarmLib {
	[Serializable()]
	public class Graph : SolutionSpace {
		protected Node[] _nodes;
		protected Hashtable _adjacency;
		// Lookup to get node by name
		private Hashtable _nodeLookup = null;

		private void BuildLookup(Node[] nodes) {
			_nodeLookup = new Hashtable();
			foreach(Node n in Nodes) {
				_nodeLookup.Add(n.GetName(), n);
			}
		}

		public Node[] Nodes {
			get { return _nodes; }
		}

		public List<Node> GetNeighbours(Node node) {
			return (List<Node>) _adjacency[node.GetName()];
		}

		public Node GetNode(string name) {
			if (_nodeLookup == null) {
				BuildLookup(_nodes);
			}
			return (Node) _nodeLookup[name];
		}

		public Graph(Node[] nodes, Hashtable adjacency) {
			_nodes = nodes;
		}

		public Graph(string[] nodeNames) {
			int size = nodeNames.Length;
			// create nodes
			_nodes = new Node[size];
			for(int i= 0; i < size; i++) {
				_nodes[i] = new Node(nodeNames[i]);
			}

			// create complete graph
			_adjacency = new Hashtable();
			for(int i = 0; i < size; i++) {
				_adjacency.Add(_nodes[i].GetName(), new List<Node>());
				for(int j = 0; j < size; j++) {
					if (i == j) continue;
					List<Node> l = (List<Node>) _adjacency[_nodes[i].GetName()];
					l.Add(_nodes[j]);
				}
			}
		}

		public override int GetSize() {
			throw new NotImplementedException();
		}
	}
}

