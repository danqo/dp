using System;
using System.Collections;
using System.Text;

namespace swarmLib {
	[Serializable()]
	public class Node : IPermutable {
		protected string _name;
		protected Hashtable _pheromones;

		public Hashtable Pheromones {
			set { _pheromones = value; }
			get { return _pheromones; }
		}

		public Node(string name) {
			_name = name;
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			foreach(DictionaryEntry i in _pheromones) {
				sb.AppendFormat("Turn {0}\n\r", i.Key);
				foreach(DictionaryEntry j in (Hashtable) i.Value){
					sb.AppendFormat("   {0} => {1}\n\r", j.Key, j.Value);
				}
			}
			return sb.ToString();
		}

		#region IPermutable implementation
		public string GetName () {
			return _name;
		}
		#endregion
	}
}

