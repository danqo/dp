using System;
using System.Collections.Generic;

namespace swarmLib {
	[Serializable()]
	public abstract class SolutionSpace {
		protected int _dimension;
		protected object[] _elements;
		public int Dimension {
			get { return _dimension; }
		}

		public object[] Elements {
			get { return _elements; }
		}

		public abstract int GetSize();

		public virtual List<Solution> GetSolutions() {
			return new List<Solution>();
		}

		public virtual int GetDistance(Solution a, Solution b) {
			object[] dataA = a.GetComponents();
			object[] dataB = b.GetComponents();
			if (a.GetLength() != b.GetLength()) {
				throw new Exception("Solutions don't have same lengths.");
			}
			int distance = 0;
			for (int i = 0; i < this.Dimension - 1; i++) {
				if (!dataA[i].Equals(dataB[i])) {
					distance++;
				}
			}
			return distance;
		}
	}
	[Serializable()]
	public abstract class Step { }
	[Serializable()]
	public abstract class StepProvider {
		public abstract void DoRandomStep(Solution solution, object[] possibilities);
		public abstract void DoStep(Solution solution, Step step);
		public abstract List<Step> FindSteps(Solution a, Solution b);

		public abstract double[] UpdateSpeed(Agent a, double[] speed, Solution gbest, double w, double c1, double c2);
		public abstract Solution UpdatePosition(SolutionSpace space, Solution solution, Solution gbest, Solution pbest, double[] speed);
		public abstract Solution UpdatePosition(SolutionSpace space, Solution a, Solution b, double alpha, double beta, double gama);
	}
}

