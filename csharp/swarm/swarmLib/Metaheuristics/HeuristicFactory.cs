using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using swarm;
using System.Collections;

namespace swarmLib {
	public enum Ciphers {
		Transposition,
		MonoalphabeticSub,
		Vinegere,
		HomophonicSub,
	}

	public enum Evals {
		BigramStat,
		MonogramStat,
		Dictionary
	}

	public class HeuristicFactory {
		public static string TEXT_DIR = "data/text/";
		public static string DICTIONARY_PATH = "data/slovak_dict.txt";
		public static string BIGRAM_STATISTIC = "data/slovak_bigrams.txt";
		public static string MONOGRAM_STATISTIC = "data/slovak_monograms.txt";
		public static int MIN_WORD_LENGTH = 3;

		private object[] _alphabet;

		public HeuristicFactory (object[] alphabet) {
			_alphabet = alphabet;
		}

		private Evaluation BuildEvaluation(ICipher cipher, Evals type) {
			Evaluation eval = null;
			switch(type) {
			case Evals.BigramStat:
				eval = new BigramEvaluation(cipher, BIGRAM_STATISTIC);
				break;
			case Evals.MonogramStat:
				eval = new MonogramEvaluation(cipher, MONOGRAM_STATISTIC);
				break;
			case Evals.Dictionary:
				eval = new DictionaryEvaluation(cipher, DICTIONARY_PATH, MIN_WORD_LENGTH);
				break;
			}
			return eval;
		}

		public Hashtable TranspositionHeuristics(int blockLen, Evals evaluation) {
			Hashtable heuristics = new Hashtable();

			ICipher cipher = new BlockTransposition(blockLen);
			Evaluation eval = BuildEvaluation(cipher, evaluation);
			Graph graph = null;
			// ACO
			{
				string[] nodeNames = new string[blockLen];
				for(int i = 0; i < blockLen; i++) nodeNames[i] = Convert.ToString(i);
				graph = new Graph(nodeNames);
				Metaheuristic aco = new ACO(eval, graph);
				heuristics.Add("aco", aco);
			}
			// PSO
			{
				PermutationSpace space = new PermutationSpace(blockLen);
				Metaheuristic pso = new PSO(eval, space);
				heuristics.Add("pso" ,pso);
			}

			// CSO
			{
				PermutationSpace space = new PermutationSpace(blockLen);
				Metaheuristic cso = new CSO(eval, space);
				heuristics.Add("cso", cso);
			}

			// FSO
			{
				PermutationSpace space = new PermutationSpace(blockLen);
				Metaheuristic fso = new FSO(eval, space);
				heuristics.Add("fso", fso);
			}

			// ABC
			{
				PermutationSpace space = new PermutationSpace(blockLen);
				Metaheuristic abc = new ABC(eval, space);
				heuristics.Add("abc" ,abc);
			}
			return heuristics;
		}

		public Hashtable SubstitutionHeuristics(Evals evaluation) {
			Hashtable heuristics = new Hashtable();

			ICipher cipher = new MonoalphabeticSubstitution();
			Evaluation eval = BuildEvaluation(cipher, evaluation);
			// ACO
			{
				Graph graph = null;
				string[] nodeNames = new string[_alphabet.Length];
				for(int i = 0; i < _alphabet.Length; i++) nodeNames[i] = Convert.ToString(_alphabet[i]);
				graph = new Graph(nodeNames);
				Metaheuristic aco = new ACO(eval, graph);
				heuristics.Add("aco", aco);
			}
			// PSO
			{
				PermutationSpace space = new PermutationSpace(_alphabet.Length);
				Metaheuristic pso = new PSO(eval, space);
				heuristics.Add("pso", pso);
			}

			// CSO
			{
				PermutationSpace space = new PermutationSpace(_alphabet.Length);
				Metaheuristic cso = new CSO(eval, space);
				heuristics.Add("cso", cso);
			}

			// FSO
			{
				PermutationSpace space = new PermutationSpace(_alphabet.Length);
				Metaheuristic fso = new FSO(eval, space);
				heuristics.Add("fso", fso);
			}

			// ABC
			{
				PermutationSpace space = new PermutationSpace(_alphabet.Length);
				Metaheuristic abc = new ABC(eval, space);
				heuristics.Add("abc" ,abc);
			}
			return heuristics;
		}

		public Hashtable VigenereHeuristics(VectorSpace space) {
			Hashtable heuristics = new Hashtable();

			ICipher cipher = new Vigenere();
			Evaluation eval = BuildEvaluation(cipher, Evals.MonogramStat);
			((MonogramEvaluation) eval).Partition = space.Dimension;
			// PSO
			{
				Metaheuristic pso = new PSO(eval, space);
				heuristics.Add("pso", pso);
			}

			// CSO
			{
				Metaheuristic cso = new CSO(eval, space);
				heuristics.Add("cso", cso);
			}

			// FSO
			{
				Metaheuristic fso = new FSO(eval, space);
				heuristics.Add("fso", fso);
			}

			// ABC
			{
				Metaheuristic abc = new ABC(eval, space);
				heuristics.Add("abc", abc);
			}
			return heuristics;
		}

		public Hashtable HomophonicSubstitutionHeuristics(PermutationSpace space, Evals evaluation, int[] frequencies) {
			Hashtable heuristics = new Hashtable();

			ICipher cipher = new HomophonicSubstitution(Utils.STA, frequencies);
			Evaluation eval = BuildEvaluation(cipher, evaluation);
			// ACO
			{
				string[] nodeNames = new string[space.Dimension];
				for(int i = 0; i < space.Dimension; i++) nodeNames[i] = Convert.ToString(i);
				Graph graph = new Graph(nodeNames);
				Metaheuristic aco = new ACO(eval, graph);
				heuristics.Add("aco", aco);
			}
			// PSO
			{
				Metaheuristic pso = new PSO(eval, space);
				heuristics.Add("pso", pso);
			}

			// CSO
			{
				Metaheuristic cso = new CSO(eval, space);
				heuristics.Add("cso", cso);
			}

			// FSO
			{
				Metaheuristic fso = new FSO(eval, space);
				heuristics.Add("fso", fso);
			}

			// ABC
			{
				Metaheuristic abc = new ABC(eval, space);
				heuristics.Add("abc", abc);
			}
			return heuristics;
		}

		public List<TestCase> ReadTestCases(string path) {
			XmlDocument doc = new XmlDocument();
			doc.Load(path);
			List<TestCase> r = new List<TestCase>();
			foreach(XmlNode node in doc.DocumentElement.SelectNodes("/run/testCase")) {
				r.Add(TestCase.Parse(node, this));
			}
			return r;
		}
	}
}