using System;
using System.Collections.Generic;

namespace swarmLib {
	public delegate Solution FindSolution(SolutionSpace space);

	public abstract class Agent : IComparable {
		protected Solution _solution;
		protected List<Agent> _connections;
		protected FindSolution _findSolution;
		protected StepProvider _stepProvider;

		public Solution Solution {
			get { return _solution; }
			set { _solution = value; }
		}

		public List<Agent> Connections {
            set { _connections = value; }
			get { return _connections; }
		}

		public FindSolution FindSolutionDelegate {
			get { return _findSolution; }
			set { _findSolution = value; }
		}

		public virtual void FindSolution(SolutionSpace space) {
			Solution = FindSolutionDelegate(space);
		}

		public virtual bool IsBetter(Solution solution) {
			if (_compare(this.Solution.Fitness, solution.Fitness) == -1) {
				return true;
			}
			return false;
		}

		public virtual int CompareTo(Object o) {
			Agent agent = (Agent) o;
			return _compare(this.Solution.Fitness, agent.Solution.Fitness);
		}

		public override string ToString() {
			return _solution.ToString();
		}

		// maximalize by default
		private int _compare(double a, double b) {
			if (a < b) {
				return 1;
			} else if (a > b) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}