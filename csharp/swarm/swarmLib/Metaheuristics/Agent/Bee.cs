using System;
using System.Collections.Generic;

namespace swarmLib {
	public class Bee : Agent {
		private int _counter;

		public int Counter {
            get { return _counter; }
			set { _counter = value; }
		}

		public Bee(StepProvider provider, FindSolution find) {
			_stepProvider = provider;
			_findSolution = find;
		}

		public void RandomStep(SolutionSpace space, object[] possibilities) {
			_stepProvider.DoRandomStep(_solution, possibilities);
		}

		public void Step(Step step) {
			_stepProvider.DoStep(_solution, step);
		}

		public List<Step> GetSteps(Solution to) {
			return _stepProvider.FindSteps(_solution, to);
		}
	}
}