using System;
using System.Collections.Generic;

namespace swarmLib {

	public class Cockroach : Agent {
		public Cockroach(StepProvider provider, FindSolution find) {
			_stepProvider = provider;
			_findSolution = find;
		}

		public void RandomStep(SolutionSpace space, object[] possibilities) {
			_stepProvider.DoRandomStep(_solution, possibilities);
		}

		public void Step(Step step) {
			_stepProvider.DoStep(_solution, step);
		}

		public List<Step> GetSteps(Solution to) {
			return _stepProvider.FindSteps(_solution, to);
		}
	}
}

