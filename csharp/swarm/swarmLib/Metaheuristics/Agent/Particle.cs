using System;
using System.Collections.Generic;

namespace swarmLib {
	public class Particle : Agent {
		Solution _pBest;
		Solution _gBest;
		Double[] _speed;

		public Solution PBest {
			get { return _pBest; }
			set { _pBest = value; }
		}

		public Solution GBest {
			get {return _gBest; }
		}

		public Double[] Speed {
			get { return _speed; }
			set { _speed = value; }
		}

		public Particle(StepProvider provider, FindSolution find) {
			_findSolution = find;
			_stepProvider = provider;
			_pBest = null;
		}

		public void SetPBest() {
			if (_pBest == null) _pBest = (Solution) this.Solution.Clone();
			if (this.IsBetter(_pBest)) {
				_pBest = (Solution) this._solution.Clone();
			}
		}

		public void SetGBest() {
			if (_gBest == null) _gBest = (Solution) this.Solution.Clone();
			foreach (Particle p in _connections) {
				if (p.IsBetter(_gBest)) {
					_gBest = (Solution) p.Solution.Clone();
				}
			}
		}

		public void UpdateSpeed(Solution gbest, double w, double c1, double c2) {
			_speed = _stepProvider.UpdateSpeed(this, _speed, gbest, w, c1, c2);
			//for(int i = 0; i< _speed.Length; i++) {
			//	_speed[i] = (PSO.SPEED_MAX - PSO.SPEED_MIM) * _speed[i]
			//}
		}

		public void UpdatePosition(SolutionSpace space) {
			this.Solution = _stepProvider.UpdatePosition(space, this.Solution, this.GBest, this.PBest, _speed);
		}
	}
}

