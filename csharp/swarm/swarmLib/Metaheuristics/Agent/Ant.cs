using System;
using System.Collections;
using System.Collections.Generic;

namespace swarmLib{

	public class Ant: Agent {
		public Ant () {
		}

		public override void FindSolution(SolutionSpace space) {
			List<string> used = new List<string>();
			Graph graph = (Graph) space;
			Node[] path = new Node[graph.Nodes.Length];
			string first = SelectNode((Hashtable) graph.Nodes[0].Pheromones[0], used);
			path[0] = graph.GetNode(first);
			used.Add(first);
			for (int i = 1; i < graph.Nodes.Length; i++) {
				Hashtable pheromones = (Hashtable) path[i -1].Pheromones[i];
				string name = SelectNode(pheromones, used);
				used.Add(name);
				path[i] = graph.GetNode(name);
			}
			_solution = new Permutation(path);
		}

		private string SelectNode(Hashtable pheromones, List<string> used) {
			List<object> available = new List<object>();
			foreach(string node in pheromones.Keys) {
				if (!used.Contains(node)) {
					available.Add(node);
				}
			}
			// sum of pheromones
			double sum = 0.0;
			foreach (string node in available) {
				sum += (double) pheromones[node];
			}
			if (sum == 0.0) sum = 1.0;

			// probabilities
			double probSum = 0.0;
			Hashtable p = new Hashtable();
			foreach (string node in available) {
				double d = (double) pheromones[node] / sum;
				p[node] = d;
				probSum += d;
			}
			Random r = new Random(DateTime.UtcNow.Millisecond);
			double selection = r.NextDouble();

			// Roulette selection
			double lb = 0.0;
			Permutation perm = new Permutation(available.Count);
			perm.SetRandom(null);
			string ret = (string) available[r.Next(0, available.Count -1)];
			foreach (string node in perm.ApplyTo(available.ToArray())) {
				lb += (double) p[node];
				if (selection <= lb) {
					ret = node;
					break;
				}
			}
			return ret;
		}

		private void UpdateNode(Node node, int turn, Node next) {
			Hashtable p = (Hashtable) node.Pheromones[turn];
			double delta = Math.Sqrt(this.Solution.Fitness) / ACO.Q;
			p[next.GetName()] = (double) p[next.GetName()] + delta;
		}

		public void UpdatePhromones(SolutionSpace space) {
			Permutation p = (Permutation) _solution;
			Node[] path = (Node[]) p.Permutable;

			UpdateNode(path[0], 0, path[0]);
			for (int i = 1; i < path.Length - 1; i++) {
				UpdateNode(path[i], i + 1, path[i + 1]);
			}
		}
	}

}