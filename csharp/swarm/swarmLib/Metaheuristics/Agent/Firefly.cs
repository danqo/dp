using System;
using System.Collections.Generic;

namespace swarmLib {
	public class Firefly : Agent {
		public Double Lucipherine {
			get { return _solution.Fitness; }
		}

		public Firefly(StepProvider sp, FindSolution find) {
			_stepProvider = sp;
			_findSolution = find;
		}

		public void RandomStep(SolutionSpace space, object[] possibilities) {
			_stepProvider.DoRandomStep(_solution, possibilities);
		}

		public void Step(Step step) {
			_stepProvider.DoStep(_solution, step);
		}

		public List<Step> GetSteps(Solution to) {
			return _stepProvider.FindSteps(_solution, to);
		}

		public void UpdatePosition(SolutionSpace space, Solution follow, double alpha, double beta, double gama) {
			this.Solution = _stepProvider.UpdatePosition(space, this.Solution, follow, alpha, beta, gama);
		}
	}
}

