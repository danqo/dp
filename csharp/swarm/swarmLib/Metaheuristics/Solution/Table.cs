using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace swarmLib {
	[Serializable()]
	public class Table : Solution {
		private Hashtable _table;

		public Table(object[] alphabet, int[] frequencies) {
			if (alphabet.Length != frequencies.Length) throw new Exception("Invalid input data");
			_table = new Hashtable();
			for (int i = 0; i < alphabet.Length; i++) {
				object row = alphabet[i];
				int columns = frequencies[i];
				_table.Add(row, new List<object>(columns));
			}
		}

		private Table(Hashtable t) {
			_table = t;
		}

		private void Set(object row, object col, object val) {
			((List<object>)_table[row])[(int)col] = val;
		}

		private object Get(object row, object col) {
			return ((List<object>)_table[row])[(int)col];
		}

		public List<object> GetRow(object row) {
			return (List<object>) _table[row];
		}

		public ICollection GetRows() {
			return _table.Keys;
		}

		public override object GetComponent(object i) {
			object[] coords = (object[]) i;
			return Get(coords[0], coords[1]);
		}

		public override object[] GetComponents() {
			throw new NotImplementedException();
		}

		public object GetComponentWithIndex(int i) {
			int temp = i;
			foreach (object r in this.GetRows()) {
				List<object> row = this.GetRow(r);
				if (temp >= row.Count) {
					temp -= row.Count;
				} else {
					return row[temp];
				}
			}
			return null;
		}

		public override void SetComponent(object i, object val) {
			object[] coords = (object[]) i;
			Set(coords[0], coords[1], val);
		}

		public object[] GetCoords(object val) {
			foreach (object r in this.GetRows()) {
				List<object> row = this.GetRow(r);
				for (int i = 0; i < row.Count; i++) {
					if (row[i].Equals(val)) {
						return new object[2] {r, i};
					}
				}
			}
			return null;
		}

		public object[] GetCoords(int index) {
			// TODO: speed up using lookup table
			int temp = index;
			foreach (object r in this.GetRows()) {
				List<object> row = this.GetRow(r);
				if (temp >= row.Count) {
					temp -= row.Count;
				} else {
					return new object[2] {r, temp};
				}
			}
			return null;
		}

		public override void SetRandom(object[] values) {
			Random r = new Random(DateTime.UtcNow.Millisecond);
			List<object> data = new List<object>(values);
			foreach(DictionaryEntry di in _table) {
				List<object> row = (List<object>)di.Value;
				for(int i = 0; i < row.Capacity; i++) {
					int index = (data.Count != 0)? r.Next(0, data.Count - 1): 0;
					row.Add(data[index]);
					data.RemoveAt(index);
				}
			}
		}

		public override object[] GetIndex(object component) {
			List<object> coords = new List<object>();
			foreach (DictionaryEntry r in _table) {
				List<object> row = (List<object>) r.Value;
				int index = 0;
				foreach (object o in row) {
					if (o.Equals(component)) {
						coords.Add(new object[] {r.Key, index});
					}
					index++;
				}
			}
			return coords.ToArray();
		}

		public override void SwapComponents(object a, object b) {
			object[] coordsA = (object[]) a;
			object[] coordsB = (object[]) b;
			object v1 = Get(coordsA[0], coordsA[1]);
			object v2 = Get(coordsB[0], coordsB[1]);
			Set(coordsA[0], coordsA[1], v2);
			Set(coordsB[0], coordsB[1], v1);
		}

		public override int GetLength() {
			return _table.Count;
		}

		public override object Clone() {
			Hashtable t = (Hashtable) _table.Clone();
			Table c = new Table(t);
			return c;
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			foreach (DictionaryEntry r in _table) {
				sb.AppendFormat("{0}: ", r.Key);
				List<object> row = (List<object>) r.Value;
				foreach (object o in row) {
					sb.AppendFormat(" {0},", o);
				}
				sb.Append("\n");
			}
			return sb.ToString();
		}

		public override bool Equals(object o) {
			Table t2 = (Table) o;
			foreach (DictionaryEntry r in _table) {
				List<object> row = (List<object>) r.Value;
				List<object> rowB = t2.GetRow(r.Key);
				if (rowB == null) {
					return false;
				}
				for (int i = 0; i < row.Count; i++) {
					if (!rowB.Contains(row[i])) {
						return false;
					}
				}
			}
			return true;
		}

		public override int GetHashCode () {
			throw new Exception("Not implemented");
		}

		public string Diff(Table t2) {
			StringBuilder sb = new StringBuilder();
			foreach (DictionaryEntry r in _table) {
				List<object> row = (List<object>) r.Value;
				List<object> rowB = t2.GetRow(r.Key);

				sb.AppendFormat("{0}: ", r.Key);
				foreach(object o in row) {
					sb.AppendFormat("{0},", o);
				}
				sb.Append("   ");
				foreach(object o in rowB) {
					sb.AppendFormat("{0},", o);
				}
				sb.Append("\n");
			}
			return sb.ToString();
		}
	}
}