using System;
using System.Collections.Generic;
using System.Text;

namespace swarmLib {
	[Serializable()]
	public class Vector : Solution {
		private object[] _data;
		private int _dimension;

		public object[] Data {
			get { return _data; }
			set { _data = value; }
		}

		public Vector(int dimension) {
			_data = new object[dimension];
			_dimension = dimension;
		}

		public Vector(int dimension, object[] data, double fitness) {
			_dimension = dimension;
			_data = data;
			_fitness = fitness;
		}

		public Vector(string keyWord) {
			int i = 0;
			_dimension = keyWord.Length;
			_data = new object[_dimension];
			foreach(char c in keyWord) {
				_data[i++] = c;
			}
		}

		public override object GetComponent(object i) {
			return _data[(int)i];
		}

		public override object[] GetComponents() {
			return this.Data;
		}

		public override object[] GetIndex(object component) {
			List<object> r = new List<object>();
			int i = 0;
			foreach(object o in _data) {
				if (o.Equals(component)) {
					r.Add(i);
				}
				i++;
			}
			return r.ToArray();
		}

		public override void SetComponent(object i, object val) {
			_data[(int)i] = val;
		}

		public override void SetRandom(object[] values) {
			Random r = new Random(DateTime.UtcNow.Millisecond);
			for(int i = 0; i < _data.Length; i++) {
				_data[i] = values[r.Next(0, values.Length - 1)];
			}
		}

		public override void SwapComponents(object a, object b) {
			object v1 = _data[(int)a];
			object v2 = _data[(int)b];
			_data[(int)a] = v2;
			_data[(int)b] = v1;
		}

		public override int GetLength() {
			return _dimension;
		}

		public override object Clone() {
			Vector v = new Vector(this._dimension, (object[]) this.Data.Clone(), this.Fitness);
			return v;
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.Append("(");
			for (int i = 0; i < _data.Length; i++) {
				sb.AppendFormat("{0} ", _data[i]);
			}
			sb.Append(")");
			return sb.ToString() + base.ToString();
		}
	}
}