using System;
using System.Text;

namespace swarmLib {
	[Serializable()]
	public abstract class Solution : ICloneable, IComparable {
		protected double _fitness;
		protected int _iteration;
		protected double _operations;

		public Double Fitness {
			get { return _fitness; }
			set { _fitness = value; }
		}

		public int Iteration {
			get { return _iteration; }
			set { _iteration = value; }
		}

		public double Operations {
			get { return _operations; }
			set { _operations = value; }
		}

		public abstract object GetComponent(object i);

		public abstract void SetComponent(object i, object val);

		public abstract void SetRandom(object[] values);

		public abstract object[] GetIndex(object component);

		public abstract void SwapComponents(object a, object b);

		public abstract int GetLength();

		public abstract object[] GetComponents();

		public override string ToString() {
			return string.Format("\nfitness: {0:F6}  iteration: {1}", _fitness, _iteration);
		}

		public abstract object Clone();

		public int CompareTo (object obj) {
			Solution b = (Solution) obj;
			if (this.Fitness < b.Fitness) {
				return 1;
			} else if (this.Fitness > b.Fitness) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}

