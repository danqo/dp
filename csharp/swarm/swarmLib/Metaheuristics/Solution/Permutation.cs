using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Linq;

namespace swarmLib
{
	//Source: http://msdn.microsoft.com/en-us/library/aa302371.aspx#permutat_topic2
	[Serializable()]
	public class Permutation : Solution {
		private IPermutable[] _permutable = null;
		private int[] data = null;
		private int order = 0;
		private int factoradicOrder = -1;

		public IPermutable[] Permutable {
			get { return _permutable; }
		}

		public Permutation(int n) {
			this.data = new int[n];
			for (int i = 0; i < n; ++i) {
				this.data[i] = i;
			}
			this.order = n;
		}

		public Permutation(IPermutable[] a) {
			_permutable = a;
			string[] basicOrder = new string[a.Length];
			int i = 0;
			foreach(IPermutable p in a) {
				basicOrder[i++] = p.GetName();
			}
			Array.Sort(basicOrder);
			int[] data = new int[a.Length];
			i = 0;
			foreach (IPermutable p in a) {
				data[i++] = Array.IndexOf(basicOrder, p.GetName());
			}
			this.data = new int[a.Length];
			data.CopyTo(this.data, 0);
			this.order = a.Length;
		}

		public Permutation(String str) {
			string[] strData = str.Split(new char[] {','});
			int[] data = new int[strData.Length];
			for (int i = 0; i < strData.Length; i++) {
				data[i] = Int32.Parse(strData[i]);
			}
			this.data = new int[data.Length];
			data.CopyTo(this.data, 0);
			this.order = data.Length;
		}

		public Permutation(int[] a) {
			this.data = new int[a.Length];
			a.CopyTo(this.data, 0);
			this.order = a.Length;
		}

		public Permutation(int n, int k) {
			this.data = new int[n];
			this.order = this.data.Length;
			factoradicOrder = k;
			// Step #1 - Find factoradic of k
			int[] factoradic = new int[n];

			for (int j = 1; j <= n; ++j) {
				factoradic[n-j] = k % j;
				k /= j;
			}
			// Step #2 - Convert factoradic to permuatation
			int[] temp = new int[n];
			for (int i = 0; i < n; ++i) {
				temp[i] = ++factoradic[i];
			}
			
			// right-most element is set to 1.
			this.data[n-1] = 1;
			for (int i = n-2; i >= 0; --i) {
				this.data[i] = temp[i];
				for (int j = i+1; j < n; ++j)
				{
					if (this.data[j] >= this.data[i])
					++this.data[j];
				}
			}
			// put in 0-based form
			for (int i = 0; i < n; ++i) {
				--this.data[i];
			}
		}

		// Clone constructor
		private Permutation(IPermutable[] permutable, int[] data, int order, int factoradicOrder, double fitness) {
			this._permutable = permutable;
			this.data = data;
			this.order = order;
			this.factoradicOrder = factoradicOrder;
			this._fitness = fitness;
		}

		public int[] GetData() {
			return (int[]) data.Clone();
		}

		public override object[] GetComponents() {
			return (object[]) data.Clone();
		}

		private static int Position(int x, bool[] mask) {
			int position  = 0;
			for (int i = 0; i < mask.Length; i++) {
				if (x==i) break;
				if (mask[i]) position++; 
			}
			mask[x] = false;
			return position;
		}
		
		private int _FindFactoradic() {
			bool[] mask = new bool[this.order];
			for(int i = 0; i < this.order; i++) mask[i] = true;
			int k = 0;
			for(int i = 0; i < this.order; i++) 
			{
				int position = Position(data[i], mask);
				
				k += position * Utils.Factorial(order - i - 1);
			}
			return k;
		}
		
		public int GetOrder() {
			if (factoradicOrder == -1)
				factoradicOrder = _FindFactoradic();
			return factoradicOrder;
		}
		
		public bool IsValid() {
			if (this.data.Length != this.order)
				return false;
			bool[] checks = new bool[this.data.Length];
			for (int i = 0; i < this.order; ++i) {
				if (this.data[i] < 0 || this.data[i] >= this.order)
					return false;  // value out of range
				if (checks[this.data[i]] == true)
					return false;  // duplicate value
				checks[this.data[i]] = true;
			}
			return true;
		}
		
		public Permutation Successor() {
			Permutation result = new Permutation(this.order);
			int left, right;
			// Step #0 - copy current data into result
			for (int k = 0; k < result.order; ++k) {
				result.data[k] = this.data[k];
			}
			// Step #1 - Find left value
			left = result.order - 2;
			while ((result.data[left] > result.data[left+1]) && (left >= 1)) {
				--left;
			}
			if ((left == 0) && (this.data[left] > this.data[left+1]))
				return null;
			// Step #2 - find right; first value > left
			right = result.order - 1;
			while (result.data[left] > result.data[right]) {
				--right;
			}
			// Step #3 - swap [left] and [right]
			int temp = result.data[left];
			result.data[left] = result.data[right];
			result.data[right] = temp;
			// Step #4 - order the tail
			int i = left + 1;       
			int j = result.order - 1;
			while (i < j) {
				temp = result.data[i];
				result.data[i++] = result.data[j];
				result.data[j--] = temp;
			}
			return result;
		}

		public object[] ApplyTo(object[] arr) {
			if (arr.Length != this.order)
				return null;
			object[] result = new object[arr.Length];
			for (int i = 0; i < result.Length; ++i) {
				result[i] = arr[this.data[i]];
			}
			return result;
		}

		public Permutation Inverse() {
			int[] inverse = new int[this.order];
			for (int i = 0; i < inverse.Length; ++i) {
				inverse[this.data[i]] = i;
			}
			return new Permutation(inverse);
		}

		public Permutation ComposeWith(Permutation p) {
			if (this.order != p.order) {
				throw new Exception("Permutations have different orders");
			}
			int[] data = new int[this.order];
			for(int i = 0; i < this.order; i++) {
				data[i] = this.data[p.data[i]];
			}
			return new Permutation(data);
		}

		public override string ToString() {
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("(");
			for (int i = 0; i < this.order; ++i) {
				sb.Append(this.data[i].ToString() + " " );
			}
			sb.Append(")");
			return sb.ToString() + base.ToString();
		}
		
		public List<Cycle> GenerateAllCycles() {
			List<Cycle> cycles = new List<Cycle>();
			int index = 0;
			bool[] mask = new bool[data.Length];
			int[] basicOrder = new int[data.Length];
			for (int j = 0; j < mask.Length; j++) {
				mask[j] = false;
				basicOrder[j] = j;
			}
			cycles.Clear();
			while (mask.Contains<bool>(false)) {
				if (!mask[index]) {
					int n = index;
					int offset = 0;
					Cycle cycle = new Cycle();
					mask[n] = true;
					cycle.Add(n);
					if (basicOrder[n] == data[n]) {
						cycles.Add(cycle);
						continue;
					}
					offset = data[n];
					while(true) {
						if (data[n] == data[offset]) {
							break;
						}
						mask[offset] = true;
						cycle.Add(offset);
						offset = data[offset];
					}
					cycles.Add(cycle);
				}
				index++;
			}
			return cycles;
		}
		
		public string CycleNotation() {
			List<Cycle> cycles = this.GenerateAllCycles();
			StringBuilder sb = new StringBuilder();
			foreach(Cycle c in cycles) {
				sb.Append(c.ToString());
			}
			return sb.ToString();
		}

		public override void SetRandom(object[] values) {
			int UNSET = -1;
			Random r = new Random(DateTime.UtcNow.Millisecond);
			int[] result = new int[order];
			for (int i = 0; i < order; i++) result[i] = UNSET;
			for (int i = 0; i < order; i++) {
				int start = r.Next(0, order -1);
				int direction = r.Next() % 2;
				while(true) {
					if (result[start] == UNSET) {
						result[start] = i;
						break;
					}
					if (direction == 0) start = (start + 1);
					if (direction == 1) start = (start - 1);
					if (start < 0) start = start + order;
					start %= order;
				}
			}
			result.CopyTo(this.data, 0);
		}

		public override object GetComponent(object i) {
			return data[(int)i];
		}

		public override void SetComponent(object i, object val) {
			throw new NotSupportedException();
		}

		public override object[] GetIndex(object component) {
			int c = (int) component;
			int i = 0;
			object[] result= null;
			foreach(int d in data) {
				if (c == d) {
					result = new object[1] {i};
					break;
				}
				i++;
			}
			return result;
		}

		public override void SwapComponents(object a, object b) {
			int aData = data[(int)a];
			int bData = data[(int)b];
			data[(int)a] = bData;
			data[(int)b] = aData;
		}

		public override int GetLength() {
			return order;
		}

		public override object Clone () {
			int[] data = new int[this.data.Length];
			Buffer.BlockCopy(this.data, 0, data, 0, this.data.Length * sizeof(int));
			//TODO: handle permutable?
			return new Permutation(this._permutable, data, order, this.GetOrder(), _fitness);
		}
	}

	public interface IPermutable {
		string GetName();
	}

	public class Cycle {
		private List<int> data;

		public Cycle () {
			data = new List<int>();
		}

		public void Add(int n) {
			data.Add(n);
		}

		public int ValueAtPosition(int position) {
			return data[position];
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.Append("(");
			for (int i = 0; i < data.Count - 1; i++) {
				sb.AppendFormat("{0} ", data[i]);
			}
			sb.AppendFormat("{0})", data[data.Count - 1]);
			return sb.ToString();
		}
	}
}