using System;
using System.Collections.Generic;
using System.Collections;

namespace swarmLib {
	public class ACO : Metaheuristic {
		public static double Q = 10.0;
		public double EVAPORATION_MIN = 0.05;
		public double EVAPORATION_MAX = 0.15;
		public double PHEROMONE_DEFAULT = 1.0;

		private Solution _gbest;

		public ACO (Evaluation eval, Graph space) {
			EVAL_COEF = Math.Pow(Math.PI/2, EVAL_COEF);
			_space = space;
			_evaluation = eval;
		}

		public override void Initialize(int agentCount, object[] closedText, IKey key) {
			base.Initialize(agentCount, closedText, key);
			InitilizePheromones(_space);
			_population = new List<Agent>(agentCount);

			for (int i = 0; i < agentCount; i++) {
				Ant ant = new Ant();
				ant.FindSolution(_space);
				ant.Solution.Fitness = this.Evaluate(ant.Solution);
				ant.UpdatePhromones(_space);
				_population.Add(ant);
			}
			_population.Sort();
			_gbest = (Solution) _population[0].Solution.Clone();
			this.AddBest(_gbest);
		}

		private void InitilizePheromones(SolutionSpace space) {
			Graph graph = (Graph) space;
			int size = graph.Nodes.Length;
			Node[] _nodes = graph.Nodes;
			// initialize pheromone trails
			Hashtable turn0 = new Hashtable();
			foreach (Node node in _nodes) {
				turn0.Add(node.GetName(), PHEROMONE_DEFAULT);
			}
			int turns = size - 1;
			// for each node
			foreach (Node node in _nodes) {
				Hashtable p = new Hashtable();
				p.Add(0, turn0);
				// turns
				for (int turn = 1; turn <= turns; turn++) {
					p.Add(turn, new Hashtable());
					// nodes
					foreach (Node neighbour in graph.GetNeighbours(node)) {
						Hashtable h = (Hashtable) p[turn];
						h.Add(neighbour.GetName(), PHEROMONE_DEFAULT);
					}
				}
				node.Pheromones = p;
			}
		}

		protected override void Iterate() {
			Graph graph = (Graph) _space;

			foreach (Ant a in _population) {
				a.FindSolution(graph);
				a.Solution.Fitness = this.Evaluate(a.Solution);
			}
			UpdatePheromones(graph);

			// check best solution
			_population.Sort();
			if (_population[0].IsBetter(_gbest)) {
				_gbest = (Solution) _population[0].Solution.Clone();
				this.AddBest(_gbest);
			}
			base.Iterate();
		}

		private void UpdatePheromones(Graph graph) {
			Random r = new Random(DateTime.UtcNow.Millisecond);
			// Evaporation
			foreach (Node n in graph.Nodes) {
				foreach (DictionaryEntry turn in n.Pheromones) {
					foreach (DictionaryEntry p in (Hashtable) turn.Value) {
						double evaporation = Utils.GetRandom(r, EVAPORATION_MIN, EVAPORATION_MAX);
						p.Value = (1 -  evaporation) * (double) p.Value;
					}
				}
			}
			// Path
			foreach (Ant a in _population) {
				a.UpdatePhromones(graph);
			}
		}
	}
}
