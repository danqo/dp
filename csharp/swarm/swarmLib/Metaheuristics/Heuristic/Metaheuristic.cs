using System;
using System.Collections.Generic;
using System.Text;

namespace swarmLib {

	public delegate bool Terminate(object sender, object args);

	public abstract class Metaheuristic {
		public int MAX_SOLUTIONS = 5;
		public const int IS_BETTER = 1;
		public double EVAL_COEF = 10;
		public int CONNECTIONS_MIN = 1;
		public int CONNECTIONS_MAX = 6;
		public int STEPS = 5;

		private double _fitness = -1.0;
		protected SolutionSpace _space;
		protected List<Agent> _population;
		protected Evaluation _evaluation;
		protected IKey _key;
		protected object[] _closedText;
		protected FindSolution _findDelegate;
		protected InitStepDecorator _initStepProvider;
		protected List<Solution> _best = new List<Solution>();
		protected List<double> _progress;
		protected List<double> _operations;
		protected double _operationsCounter;
		protected int _iteration;
		protected Random _random;

		protected void AddBest(Solution val) {
			val.Iteration = _iteration;
			val.Operations = _operationsCounter;
			_best.Add(val);
			_best.Sort();
			Console.WriteLine(val.ToString());
			if (_best.Count >= MAX_SOLUTIONS) {
				_best.RemoveAt(_best.Count - 1);
			}
		}

		public string Name {
			get {
				string[] name = base.GetType().ToString().Split(new char[]{'.'});
				return name[name.Length - 1].ToLower();
			}
		}

		public string CipherName {
			get {
				string[] name = _evaluation.Cipher.GetType().ToString().Split(new char[]{'.'});
				return name[name.Length - 1];
			}
		}

		public double KeyFitness {
			get {
				if (_fitness == -1.0) {
					_fitness = _evaluation.Evaluate(_closedText, _key);
				}
				return _fitness;
			}
		}

		public List<Solution> Best {
			get { return _best; }
		}

		public List<double> Progress {
			get { return _progress; }
		}

		public List<double> Operations {
			get { return _operations; }
		}

		public Evaluation Evaluation {
			get { return _evaluation; }
			set { _evaluation = value; }
		}

		public SolutionSpace Space {
			get { return _space; }
		}

		public virtual Solution GetBest() {
			_population.Sort();
			return _population[0].Solution;
		}

		public virtual void InitConnections(int min, int max) {
			Random r = new Random(DateTime.UtcNow.Millisecond);
			for(int i = 0; i < _population.Count; i++) {
				Agent agent = _population[i];
				int count = r.Next(min, max);
				List<int> filter = new List<int>() {i};
				List<int> selection = Utils.GetRandomSelection(_population, count, filter);
				List<Agent> connections = new List<Agent>();
				foreach(int j in selection) {
					connections.Add(_population[j]);
				}
				agent.Connections = connections;
			}
		}

		public virtual void Initialize(int agentCount, object[] closedText, IKey key) {
			_key = key;
			_closedText = closedText;
			_operationsCounter = 0;
			_iteration = 0;
			_progress = new List<double>(8000);
			_best.Clear();
			_operations = new List<double>(8000);
			_random = new Random(DateTime.Now.Millisecond);
			CONNECTIONS_MIN = Convert.ToInt32(agentCount  * 0.05);
			if (CONNECTIONS_MIN == 0) CONNECTIONS_MIN  = 1;
			CONNECTIONS_MAX = Convert.ToInt32(agentCount * 0.2);
			Console.WriteLine("Finding: {0}", Evaluate(_key));
		}

		protected virtual void Iterate() {
			_iteration++;
		}

		private double CheckFitnessRange(double d) {
			if (d == Double.PositiveInfinity) return Double.MaxValue;
			if (d == Double.NegativeInfinity) return Double.MinValue;
			if (Double.IsNaN(d)) throw new Exception("Fitness value == NaN");
			return d;
		}

		protected virtual double Evaluate(IKey key) {
			_operationsCounter++;
			return CheckFitnessRange(_evaluation.Evaluate(_closedText, key));
		}

		protected virtual double Evaluate(Solution solution) {
			if (_operationsCounter % 10 == 0 && _operationsCounter != 0) {
				double best = (_best.Count == 0)? 0.0 :_best[0].Fitness;
				_progress.Add(best);
				_operations.Add(_operationsCounter);
			}
			_operationsCounter++;
			return CheckFitnessRange(_evaluation.Evaluate(_closedText, solution));
		}

		public virtual void Run(int iterations) {
			for (int i = 0; i < iterations; i++) {
				Iterate();
			}
		}

		public virtual void Run(Terminate terminate, int max) {
			int counter = 0;
			while(!terminate(this, null) && (counter < max)) {
				Iterate();
				counter++;
			}
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Metaheuristic: " + this.Name);
			sb.AppendLine("Cipher: " + _evaluation.Cipher.ToString());
			foreach(Solution a in _best) {
				sb.AppendLine(a.ToString());
			}
			sb.AppendFormat("\nTerminated: {0}", _iteration);
			sb.AppendFormat("\nKey: {0}", _key.ToString());
			sb.AppendFormat("\nFitness: {0}", _evaluation.Evaluate(_closedText, _key));
			//sb.AppendLine("Text:\n");
			//object[] decrypted = _evaluation.Cipher.Decrypt(_closedText, _key);
			//foreach(object o in decrypted) sb.Append(o);
			sb.AppendLine("\n------------------");
			return sb.ToString();
		}

		public string TestEvaluation() {
			StringBuilder sb = new StringBuilder();
			int i = 0;
			foreach(Solution k in _space.GetSolutions()) {
				double val = _evaluation.Evaluate(_closedText, k);
				sb.AppendFormat("{0},{1},{2}\n", i, val, k.ToString());
				i++;
			}
			return sb.ToString();
		}

		public static Solution FindPermutation(SolutionSpace space) {
			Permutation p = new Permutation(space.Dimension);
			p.SetRandom(null);
			return p;
		}

		public static Solution FindKeyWord(SolutionSpace space) {
			Vector v = new Vector(space.Dimension);
			v.SetRandom(space.Elements);
			return v;
		}

		public static Solution FindTable(SolutionSpace space) {
			TableSpace ts = (TableSpace) space;
			Table t = new Table(ts.Alphabet, ts.Frequencies);
			t.SetRandom(space.Elements);
			return t;
		}

	    // Step providers init methods
		public static StepProvider PermutationStep() {
			return new PermutationStepProvider();
		}

		public static StepProvider VectorStep() {
			return new VectorStepProvider();
		}

		public static StepProvider TableStep() {
			return new TableStepProvider();
		}

		// Terminators
		public static bool TerminatCheck(object sender, object args) {
			double treshold = 2.7;
			Metaheuristic m = (Metaheuristic) sender;
			List<Solution> best = m.Best;
			if (best.Count <= 3) return false;
			double sum = 0.0;
			double avg = 0.0;
			foreach (Solution s in best) {
				sum += s.Fitness;
			}
			avg = sum / best.Count;
			foreach (Solution s in best) {
				if ((s.Fitness / avg) > treshold) {
					return true;
				}
			}
			return false;
		}
	}
}

