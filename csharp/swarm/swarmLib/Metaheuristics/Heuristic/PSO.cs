using System;
using System.Collections.Generic;

namespace swarmLib {
	public class PSO : Metaheuristic {
		public static double SPEED_MIM = -1000.0;
		public static double SPEED_MAX = 1000.0;

		private Solution _gbest;

		public PSO(Evaluation eval, PermutationSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindPermutation;
			_initStepProvider = Metaheuristic.PermutationStep;
		}

		public PSO(Evaluation eval, VectorSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindKeyWord;
			_initStepProvider = Metaheuristic.VectorStep;
		}

		public PSO(Evaluation eval, TableSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindTable;
			_initStepProvider = Metaheuristic.TableStep;
		}

		public override void Initialize(int agentCount, object[] closedText, IKey key) {
			base.Initialize(agentCount, closedText, key);
			_population = new List<Agent>(agentCount);
			for (int i = 0; i < agentCount; i++) {
				Particle p = new Particle(_initStepProvider(), _findDelegate);
				_population.Add(p);
			}
			InitConnections(CONNECTIONS_MIN, CONNECTIONS_MAX);
			// Initialize position and fitness
			foreach(Particle p in _population) {
				p.FindSolution(_space);
				p.Solution.Fitness = this.Evaluate(p.Solution);
			}

			// Check pbest
			foreach(Particle p in _population) {
				p.SetPBest();
			}
			// Init gbest
			Particle p0 = (Particle) _population[0];
			_gbest = (Solution) p0.PBest.Clone();
			foreach(Particle p in _population) {
				if (p.IsBetter(_gbest)) {
					_gbest = (Solution) p.Solution.Clone();
				}
			}
			this.AddBest(_gbest);
			// Initialize speed
			foreach(Particle p in _population) {
				double[] speed = Utils.GetRandom(-1 * _space.Dimension, _space.Dimension, _space.Dimension);
				p.Speed = speed;
			}
		}

		protected override void Iterate() {
			Random r = new Random(DateTime.UtcNow.Millisecond);
			double w = 0.721;
			double c1 = Utils.GetRandom(r ,0.0, 1.193);
			double c2 = Utils.GetRandom(r, 0.0, 1.193);
			_population[0].Solution = (Solution) _gbest.Clone();
			foreach(Particle p in _population) {
				p.SetPBest();
				p.SetGBest();
				p.UpdateSpeed(_gbest, w, c1, c2);
				p.UpdatePosition(_space);
				p.Solution.Fitness = Evaluate(p.Solution);

				if (p.IsBetter(_gbest)) {
					_gbest = (Solution) p.Solution.Clone();
					this.AddBest(_gbest);
				}
			}
			InitConnections(CONNECTIONS_MIN, CONNECTIONS_MAX);
			if (_iteration % 150 == 0 && _iteration != 0) {
				foreach(Particle p in _population) {
					double[] speed = new double[_space.Dimension];
					p.Speed = speed;
				}
			}
			base.Iterate();
		}
	}
}

