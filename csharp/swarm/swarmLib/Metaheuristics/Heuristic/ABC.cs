using System;
using System.Collections.Generic;

namespace swarmLib {
	public class ABC : Metaheuristic {
		public int DIVIDE_RATIO = 3;
		public int WORKERS_COUNT;
		public int EXPLORER_COUNT;
		public int WAITERS_COUNT;
		public int LEAVE_LIMIT = 3;

		private List<Agent> _workers;
		private List<Agent> _explorers;
		private List<Agent> _waiters;

		private Solution _optimum;

		public ABC(Evaluation eval, PermutationSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindPermutation;
			_initStepProvider = Metaheuristic.PermutationStep;
		}

		public ABC(Evaluation eval, VectorSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindKeyWord;
			_initStepProvider = Metaheuristic.VectorStep;
		}

		public ABC(Evaluation eval, TableSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindTable;
			_initStepProvider = Metaheuristic.TableStep;
		}

		public override void Initialize(int agentCount, object[] closedText, IKey key) {
			base.Initialize(agentCount, closedText, key);
			LEAVE_LIMIT = _space.Dimension / 4;
			LEAVE_LIMIT = (LEAVE_LIMIT == 0)? 1: LEAVE_LIMIT;
			_population = new List<Agent>(agentCount);
			InitializeBees(agentCount);
			_population.Sort();
			this.AddBest(_population[0].Solution);
		}

		private void InitializeBees(int count) {
			EXPLORER_COUNT = count / DIVIDE_RATIO;
			WORKERS_COUNT = count / DIVIDE_RATIO;
			WAITERS_COUNT = count - (EXPLORER_COUNT + WORKERS_COUNT);

			// Workers
			_workers = new List<Agent>(WORKERS_COUNT);
			for(int i = 0; i < WORKERS_COUNT; i++) {
				Bee b = new Bee(_initStepProvider(), _findDelegate);
				b.FindSolution(_space);
				b.Solution.Fitness = Evaluate(b.Solution);
				_workers.Add(b);
				_population.Add(b);
			}
			// Explorers
			_explorers = new List<Agent>(EXPLORER_COUNT);
			for(int i = 0; i < EXPLORER_COUNT; i++) {
				Bee b = new Bee(_initStepProvider(), _findDelegate);
				_explorers.Add(b);
				_population.Add(b);
				b.FindSolution(_space);
				b.Solution.Fitness = Evaluate(b.Solution);
			}
			_explorers.Sort();
			// Waiters
			_waiters = new List<Agent>(WAITERS_COUNT);
			for(int i = 0; i < WAITERS_COUNT; i++) {
				Bee b = new Bee(_initStepProvider(), _findDelegate);
				_waiters.Add(b);
				_population.Add(b);
				b.FindSolution(_space);
				b.Solution.Fitness = Evaluate(b.Solution);
			}
			_optimum = (Solution) _workers[0].Solution.Clone();
		}

		protected override void Iterate() {
			List<double> data = new List<double>(_workers.Count);
			// Workers job
			// Improvement: first worker always works on current optimum
			_workers[0].Solution = (Solution) _optimum.Clone();
			foreach(Bee b in _workers) {
				Solution before = (Solution) b.Solution.Clone();
				b.RandomStep(_space, _space.Elements);
				b.Solution.Fitness = Evaluate(b.Solution);
				data.Add(b.Solution.Fitness);
				b.Counter++;
				// Reset counter for successfull bees
				if (b.IsBetter(before)) {
					b.Counter = 0;
				}
			}
			// Waiters job, dancing
			foreach(Bee b in _waiters) {
				int follow = Utils.RouleteSelection(data.ToArray());
				Bee follower = (Bee) _workers[follow];
				b.Solution = (Solution) _workers[follow].Solution.Clone();
				int swaps = _random.Next(1, 5);
				for (int i = 0; i < swaps; i++) {
					b.RandomStep(_space, _space.Elements);
				}
				b.Solution.Fitness = Evaluate(b.Solution);
				// Reset counter for successfull follower
				if (b.IsBetter(follower.Solution)) {
					follower.Counter = 0;
				}
			}
			// Explorers job
			foreach(Bee b in _explorers) {
				Solution old = (Solution) b.Solution.Clone();
				b.FindSolution(_space);
				b.Solution.Fitness = Evaluate(b.Solution);
				if (!b.IsBetter(old)) {
					b.Solution = old;
				}
			}
			_explorers.Sort();
			// Leave unperspective places
			for (int i = 0; i < _workers.Count; i++) {
				Bee b = (Bee) _workers[i];
				// Switch with first explorer
				if (b.Counter >= LEAVE_LIMIT) {
					b.Counter = 0;
					Bee w = (Bee) _explorers[0];
					_explorers.RemoveAt(0);
					_explorers.Add(b);
					_workers[i] = w;
				}
			}
			// Update optimum
			foreach (Bee b in _population) {
				if (b.IsBetter(_optimum)) {
					_optimum = (Solution) b.Solution.Clone();
					this.AddBest(_optimum);
				}
			}
			base.Iterate();
		}
	}
}