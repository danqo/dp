using System;
using System.Collections.Generic;

namespace swarmLib {
	public class FSO : Metaheuristic {
		public static double LUCIPHERINE_KOEF = 0.6;
		public static double LUCIPHERINE_EVAPOR = 0.4;
		public static double LUCIPHERINE_MIN = 0.0;
		public static double LUCIPHERINE_MAX = 1.0;
		public static double ABSORBTION = 0.3;

		private Solution _optimum;
		private Random r = new Random(DateTime.UtcNow.Millisecond);

		public FSO(Evaluation eval, PermutationSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindPermutation;
			_initStepProvider = Metaheuristic.PermutationStep;
		}

		public FSO(Evaluation eval, TableSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindTable;
			_initStepProvider = Metaheuristic.TableStep;
		}

		public FSO(Evaluation eval, VectorSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindKeyWord;
			_initStepProvider = Metaheuristic.VectorStep;
		}

		public override void Initialize(int agentCount, object[] closedText, IKey key) {
			base.Initialize(agentCount, closedText, key);
			_population = new List<Agent>(agentCount);
			for (int i = 0; i < agentCount; i++) {
				Firefly p = new Firefly(_initStepProvider(), _findDelegate);
				p.FindSolution(_space);
				p.Solution.Fitness = Evaluate(p.Solution);
				_population.Add(p);
			}
			InitConnections(CONNECTIONS_MIN, CONNECTIONS_MAX);
			_optimum = (Solution) _population[0].Solution.Clone();
			this.AddBest(_optimum);
		}

		protected override void Iterate() {
			double alpha = 0.01 * _space.Dimension;
			double beta = _space.Dimension;
			double gama = 1.0 / Math.Sqrt(_space.Dimension);
			// Improvement: first firefly always works on current optimum
			_population[0].Solution = (Solution) _optimum.Clone();
			foreach(Firefly g in _population) {
				// Search for highest light intensity
				double[] lights = new double[g.Connections.Count];
				int i = 0;
				foreach(Firefly h in g.Connections) {
					int distance = g.GetSteps(h.Solution).Count;
					distance = (distance == 0) ? 1 : distance;
					lights[i++] = (h.Lucipherine / (distance * distance * ABSORBTION)) ;
				}

				// Goes towards highest light intensity or do random step
				Solution best = g.Connections[Utils.GetMax(lights)].Solution;
				if (!g.IsBetter(best)) {
					double rand = r.NextDouble();
					g.UpdatePosition(_space, best, alpha, beta, (-1 * rand  * rand * gama));
				} else {
					g.RandomStep(_space, _space.Elements);
				}
				g.Solution.Fitness = Evaluate(g.Solution);
				if (g.IsBetter(_optimum)) {
					_optimum = (Solution) g.Solution.Clone();
					this.AddBest(_optimum);
				}
			}

			// Set actual optimum
			foreach (Firefly g in _population) {
				if (g.IsBetter(_optimum)) {
					_optimum = (Solution) g.Solution.Clone();
					this.AddBest(_optimum);
				}
			}
			InitConnections(CONNECTIONS_MIN, CONNECTIONS_MAX);
			base.Iterate();
		}
	}
}

