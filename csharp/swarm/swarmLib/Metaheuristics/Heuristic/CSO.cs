using System;
using System.Collections.Generic;

namespace swarmLib {
	public delegate StepProvider InitStepDecorator();

	public class CSO : Metaheuristic {
		private Solution _lof;

		public Solution LOF {
			set {
				_lof = value;
				this.AddBest((Solution) value.Clone());
			}
			get { return _lof; }
		}

		public CSO (Evaluation eval, PermutationSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindPermutation;
			_initStepProvider = Metaheuristic.PermutationStep;
		}

		public CSO(Evaluation eval, VectorSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindKeyWord;
			_initStepProvider = Metaheuristic.VectorStep;
		}

		public CSO(Evaluation eval, TableSpace space) {
			_evaluation = eval;
			_space = space;
			_findDelegate = Metaheuristic.FindTable;
			_initStepProvider = Metaheuristic.TableStep;
		}

		public override void Initialize(int agentCount, object[] closedText, IKey key) {
			base.Initialize(agentCount, closedText, key);
			STEPS = _space.Dimension;
			_population = new List<Agent>(agentCount);
			this.LOF = _findDelegate(_space);
			this.AddBest(this.LOF);
			this.LOF.Fitness = Evaluate(this.LOF);
			for (int i = 0; i < agentCount; i++) {
				Cockroach p = new Cockroach(_initStepProvider(), _findDelegate);
				p.Solution = (Solution) this.LOF.Clone();
				_population.Add(p);
			}
		}

		protected override void Iterate() {
			foreach(Cockroach c in _population) {
				for(int i = 0; i < STEPS; i++) {
					c.RandomStep(_space, _space.Elements);
					c.Solution.Fitness = Evaluate(c.Solution);
					if (c.IsBetter(this.LOF)) {
						this.LOF = (Solution) c.Solution.Clone();
					}
				}
			}
			foreach(Cockroach c in _population) {
				List<Step> steps = c.GetSteps(this.LOF);
				foreach(Step step in steps) {
					c.Step(step);
					c.Solution.Fitness = Evaluate(c.Solution);
					if (c.IsBetter(this.LOF)) {
						this.LOF = (Solution) c.Solution.Clone();
						break;
					}
				}
			}
			base.Iterate();
		}
	}
}

