using System;
using System.IO;
using System.Collections;
using System.Text;

namespace swarmLib
{
	public class NgramStatistic {
		public static double SAMPLE_SIZE = 1000.0;
		private Hashtable _ngrams;
		
		public NgramStatistic (string path) {
			_ngrams = new Hashtable();
			Load(path);
		}

		public Hashtable NGrams {
			get { return _ngrams; }
		}
		
		private void Load(string path) {
			char[] separator = new char[1] {','};
			using(StreamReader sr = new StreamReader(path)) {
				string line;
				while ((line = sr.ReadLine()) != null) {
					string[] pair = line.Split(separator);
					double val = double.Parse((string) pair[1]);
					_ngrams.Add(pair[0], val);
				}
			}
		}

		public Double Compare(int n, object[] text) {
			// realtive ngrams in text
			Hashtable relatives = GetRelativeNgrams(n, text);

			// compare with statistic
			Hashtable result = new Hashtable();
			Hashtable diffs = (Hashtable) _ngrams.Clone();
			double cumulativeDev = 0.0;
			foreach(DictionaryEntry pair in _ngrams) {
				double stat = (double)  pair.Value;
				double val = 0.0;
				if (relatives.ContainsKey(pair.Key)) { val = (double) relatives[pair.Key]; }
				double diff = val - stat;
				if (diff < 0) { diff *= (double) -1.0; }
				cumulativeDev += diff;
				diffs[pair.Key] = diff;
			}
			result.Add("diff", diffs);
			result.Add("cumulativeDev", cumulativeDev);
			return (double) result["cumulativeDev"];
		}

		#region static methods

		private static Hashtable GetNgrams(int n, object[] input) {
			return (Hashtable) GenerateNgrams(n, input)["ngrams"];
		}

		private static Hashtable GetRelativeNgrams(int n, object[] input) {
			Hashtable ngrams = GenerateNgrams(n, input);
			Hashtable result = new Hashtable();
			foreach(DictionaryEntry pair in (Hashtable) ngrams["ngrams"]) {
				result[pair.Key] = (double) ((double) pair.Value / input.Length * NgramStatistic.SAMPLE_SIZE / n);
			}
			return result;
		}

		private static Hashtable GenerateNgrams(int n, object[] input) {
			Hashtable ngrams = new Hashtable();
			double total = 0.0;
			for (int i = 0; i < input.Length - 1; i++) {
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < n; j++) sb.Append((char) input[i+j]);
			    string key = sb.ToString();
				if (!ngrams.ContainsKey((string) key)) {
					ngrams.Add((string) key, (double) 1.0);
				} else {
					double val= (double) ngrams[(string)key];
					val += 1.0;
					ngrams[(string)key] = val;
				}
				total += 1.0;
			}
			Hashtable result = new Hashtable();
			result.Add("ngrams", ngrams);
			result.Add ("total", total);
			return result;
		}

		public static void Save(string path, Hashtable ngrams) {
			using (StreamWriter sw = new StreamWriter(path)) {
				foreach (DictionaryEntry pair in ngrams) {
					sw.WriteLine("{0},{1}", pair.Key, pair.Value);
				}
				sw.Close();
			}
		}

		public static Hashtable LoadFile(string path) {
			Hashtable ngrams = new Hashtable();
			using (StreamReader sr = new StreamReader(path)){
				char[] separator = new char[1] {','};
				string line;
				while ((line = sr.ReadLine()) != null) {
					string[] pair = line.Split(separator, new StringSplitOptions());
					ngrams.Add((string) pair[0], (int) int.Parse(pair[1]));
				}
			}
			return ngrams;
		}

		#endregion
	}
}

