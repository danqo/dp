using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace swarmLib
{
	public static class Utils {
		private static Random random = new Random();
		public static object[] STA = {
			'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z'
		};

		// Freguencies per 100 characters
		public static int[] STA_FREQUENCIES = {
			10, 1, 4, 3, 8, 1,
			1, 2, 7, 2, 4, 4,
			3, 6, 10,3, 1, 3,
			5, 6, 4, 5, 1, 1,
			2, 3,
		};

		public static int Factorial(int i) {
			return i < 0 ? -1 : i == 0 || i == 1 ? 1 : Enumerable.Range(1, i).Aggregate((counter, value) => counter * value);
		}

		public static object[] StringToObjectsArray(string str) {
			object[] result = new object[str.Length];
			int i = 0;
			foreach(char s in str.ToCharArray()) {
				result[i++] = s;
			}
			return result;
		}
		
		public static char[] ObjectArrayToCharArray(object[] data) {
			char[] result = new char[data.Length];
			int i = 0;
			foreach(object o in data) {
				result[i++] = (char) o;
			}
			return result;
		}

		public static double GetRandom(double min, double max) {
			return GetRandom(random, min, max);
		}

		public static double GetRandom(Random r, double min, double max) {
			return (r.NextDouble() * (max - min)) + min;
		}

		public static double[] GetRandom(double min, double max, int length) {
			double[] data = new double[length];
			for(int i = 0; i < data.Length; i++) {
				data[i] = GetRandom(random, min, max);
			}
			return data;
		}

		public static int RouleteSelection(double[] data) {
			double sum = 0.0;
			foreach(double d in data) {
				sum += (d < 0.0) ? -1.0 * d : d;
			}
			double[] probs = new double[data.Length];
			for(int i = 0; i < data.Length; i++) {
				double val = (data[i] < 0.0) ? -1.0 * data[i] : data[i];
				probs[i] = val / sum;
			}
			Random r = new Random();
			double selection = r.NextDouble();
			double lb = 0.0;
			int ret = 0;
			for(int i = 0; i < data.Length; i++) {
				lb += probs[i];
				if (selection < lb) {
					ret = i;
					break;
				}
			}
			return ret;
		}

		public static int GetMax(double[] data) {
			double max = Double.MinValue;
			int index = 0;
			for(int i = 0; i < data.Length; i++) {
				if(data[i] >= max) {
					max = data[i];
					index = i;
				}
			}
			return index;
		}

		public static List<int> GetRandomSelection(IList array, int count, List<int> filter) {
			if (array.Count - filter.Count < count) {
				throw new Exception("Infinite loop");
			}
			Random r = new Random();
			List<int> result = new List<int>();
			do {
				int s = r.Next(0, array.Count);
				if (filter.Contains(s)) continue;
				if (result.Contains(s)) continue;
				result.Add(s);
			} while(result.Count < count);
			return result;
		}

		public static object[] ReadFile(string path) {
			StreamReader sr = new StreamReader(path);
			string input = sr.ReadToEnd();
			object[] text = new object[input.Length];
			int i = 0;
			foreach (char c in input) { text[i++] = c; }
			return text;
		}
	}
}